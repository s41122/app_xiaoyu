<?php

class Log {
    public static function LogWirte($Astring) {
        $file = EXTEND_PATH . "baofu/log/" . date('Ymd', time()) . ".txt";
        $LogTime = date('Y-m-d H:i:s', time());
        if (!file_exists($file)) {
            $logfile = fopen($file, "w") or die("Unable to open file!");
            fwrite($logfile, "[$LogTime]:" . $Astring . "\r\n");
            fclose($logfile);
        } else {
            $logfile = fopen($file, "a") or die("Unable to open file!");
            fwrite($logfile, "[$LogTime]:" . $Astring . "\r\n");
            fclose($logfile);
        }
    }
}