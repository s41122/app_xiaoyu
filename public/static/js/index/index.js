$(function () {
    //登录
    $('#btn-login').click(function () {
        var phone = $('#phone').val();
        if (phone == '') {
            mui.alert("请输入手机号码");
            return false;
        }
        if (!isMobile(phone)) {
            mui.alert('手机号码格式有误!');
            return false;
        }
        var pwd = $('#pwd').val();
        if (pwd == '') {
            mui.alert("请输入密码");
            return false;
        }
        if (pwd.length < 6 || pwd.length > 15) {
            mui.alert('密码长度必须为6-15位!');
            return false;
        }
        $.ajax({
            url: "/login/login",
            type: 'POST',
            dataType: 'json',
            data: {phone:phone, pwd: pwd},
            success: function (data) {
                if (data.code == 1) {
                    mui.alert('登录成功!');
                    location.href = '/account';
                } else {
                    mui.alert('登录失败!');
                }
            }
        })
    })

    //注册发送手机验证码
    $('.yzm_true').click(function () {
        var o = $(this);
        var phone = $("#phone").val();
        if (phone == '') {
            mui.alert("请输入手机号码");
            return false;
        }
        if (!isMobile(phone)) {
            mui.alert('手机号码格式有误!');
            return false;
        }
        var wait = 60;
        $.ajax({
            url: '/reg/getcode',
            type: 'post',
            data: {phone: phone},
            dataType: 'json',
            success: function (data) {
                if (data.code == 1) {
                    mui.alert("发送成功");
                    var timeID = setInterval(function () {
                        o.html(" 等待" + wait + "秒 ");
                        o.attr("disabled", "true");
                        wait = wait - 1;
                        if (wait == -1) {
                            clearInterval(timeID);
                            o.html("获取验证码");
                            o.removeAttr("disabled");
                        }
                    }, 1000);
                } else {
                    mui.alert(data.msg);
                }
            }
        })
    })

    //注册第一步
    $("#reg-first").click(function () {
        var phone = $("#phone").val();
        var code = $("#code").val();
        if (phone == '') {
            mui.alert("请输入手机号码");
            return false;
        }
        if (!isMobile(phone)){
            mui.alert("手机号码格式有误");
            return false;
        }
        if (code == '') {
            mui.alert("请输入手机验证码");
            return false;
        }
        if (!$("#zf01").is(':checked')) {
            mui.alert("请先同意注册协议进行注册");
            return false;
        }
        $.ajax({
            url: '/reg/reg',
            type: 'post',
            data: $('form').serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.code == 1) {
                    location.href = '/reg/second';
                } else {
                    mui.alert(data.msg);
                }
            }
        })
    })

    //注册第二步
    $("#reg-second").click(function () {
        var pwd = $("#pwd").val();
        var cpwd = $("#cpwd").val();
        var inviter = $("#inviter").val();
        if (pwd == '') {
            mui.alert("请输入密码");
            return false;
        }
        if (pwd.length < 6 || pwd.length > 15) {
            mui.alert("密码长度必须为6-15位!");
            return false;
        }
        if (pwd != cpwd) {
            mui.alert("密码不一致!");
            return false;
        }
        $.ajax({
            url: '/reg/second',
            type: 'post',
            data: {pwd:pwd, cpwd:cpwd, inviter:inviter},
            dataType: 'json',
            success: function (data) {
                if (data.code == 1) {
                    mui.alert('恭喜您注册成功!');
                    location.href = '/account';
                } else {
                    mui.alert(data.msg);
                }
            }
        })
    })
})