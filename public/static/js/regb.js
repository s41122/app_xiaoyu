$(".h5_bnt").click(function () {
    if ($(".checked").attr("checked")) {
        var phone = $(".h5b h2").text()
        var yzm = $(".yzm").val()
        var pwd = $(".pwd").val()
        var pwd1 = $(".pwd1").val()
        var url = $(this).data('url')
        var inviter = $(this).data('inviter')
        if (!yzm) {
            mui.alert("请输入验证码")
        } else if (!pwd) {
            mui.alert("请输入密码")
        } else if (pwd != pwd1) {
            mui.alert("两次输入密码不一致")
        } else {
            mui.post(url, {
                phone: phone,
                code: yzm,
                pwd: pwd,
                inviter: inviter
            }, function(data) {
                if(data.code == 1) {
                    // mui.alert(data.msg);
                    $('.win_box').show();
                } else {
                    mui.alert(data.msg);
                }
            })
        }
    } else {
        mui.alert("请同意注册条款");
    }
})

$(".yzm_true").click(function () {
    var phone = $("#phone").html();
    var url = $(this).data('url');
    mui.post(url, {
        phone: phone
    }, function(data) {
        if(data.code == 1) {
            mui.alert(data.msg);
            //60s后重新获取验证码
            var wait = 60;
            var timeID = setInterval(function() {
                $('.yzm_true').html("等待" + wait + "秒 ");
                $('.yzm_true').addClass("yzm_no");
                $('.yzm_true').attr("disabled", true);
                wait = wait - 1;
                if(wait == -1) {
                    clearInterval(timeID);
                    $('.yzm_true').html(" 免费获取 ");
                    $('.yzm_true').removeClass("yzm_no");
                    $('.yzm_true').attr("disabled", false);
                }
            }, 1000);
        } else {
            alert(data.msg);
        }
    })
})