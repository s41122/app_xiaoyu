<?php
namespace app\common\logic;

use think\Model;

class User extends Model {
    public function login($username, $pwd) {
        if (!$username || !$pwd) {
            return false;
        }
        $userModel = model('user');
        $where ['phone|username'] = $username;
        $user = $userModel->getUser($where);
        if (!$user) {
            return $this->loginCode(2);
        }
        if ($user['password'] != md5($pwd)) {
            return $this->loginCode(3);
        }
        if ($user['block_status'] == 1) {
            return $this->loginCode(4);
        }

        //更新用户登录信息
        $re = $userModel->updateUserLogin($user['user_id']);
        if (!$re) {
            return $this->loginCode(5);
        }

        //加入用户操作记录
        model('users_log')->addUserLog([
                'user_id' => $user['user_id'],
                'code' => 'users',
                'type' => 'action',
                'operating' => 'login',
                'article_id' => $user['user_id'],
                'result' => 1,
                'content' => date('Y-m-d H:i:s') . ' 登录成功'
        ]);

        //记录cookie
        $cdata['user_id'] = $user['user_id'];
        $cdata['cookie_status'] = config('con_cache_type'); //1:cookie 0:session
        $cdata["cookie_id"] = config('con_cookie_id');
        $cdata['time'] = $cdata['cookie_status'] == 1 ? 1800 : 86400;
        SetCookies($cdata);

        return $this->loginCode(1);
    }

    public function loginCode($code = 1) {
        switch ($code) {
            case 1:
                $msg = '成功';
                break;
            case 2:
                $msg = '用户不存在';
                break;
            case 3:
                $msg = '密码不正确';
                break;
            case 4:
                $msg = '对不起,您的帐号已被锁定,请联系在线客服';
                break;
            case 5:
                $msg = '更新用户登录信息失败!';
                break;
            default:
                $msg = '登录失败';
        }
        return ['code' => $code, 'msg' => $msg];
    }

    public function logout() {
        $data['cookie_status'] = config('con_cache_type'); //1:cookie 0:session
        $data["cookie_id"] = config('con_cookie_id');
        DelCookies($data);
        return ['code' => 1, 'msg' => ''];
    }


    /**
     * 添加用户
     * @param Array $index
     * @param $user_id 返回用户ID
     * @return Boolen
     */
    static function AddUser($data = []) {
        $user_id = model('user')->addUser($data);

        if (!$user_id) {
            return ['code' => 0, 'msg' => '注册失败,请重试!'];
        } else {
            //用户信息表（users_info）初始化
            $re = model('users_info')->edit([
                    'user_id' => $user_id,
                    'status' => 1,
                    'type_id' => 1,

            ]);

            $unique = model('unique', 'logic');
            $invite_code = $unique::get($user_id, 6);

            $sql = "insert into `{users_info}` set status='{$data['status']}',user_id='{$user_id}',type_id=1, phone='" . $data['phone'] . "', phone_status=1,invite_code=" . $invite_code;
            $mysql->db_query($sql);
            $sql = "insert into `{approve_sms}` set `addtime` = '" . time() . "',`addip` = '" . get_client_ip() . "',user_id='{$user_id}',status=1,`phone`='{$data['phone']}', credit=0, verify_userid=0, verify_remark=" . time();
            $mysql->db_query($sql);
        }
    }
}
