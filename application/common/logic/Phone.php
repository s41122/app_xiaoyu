<?php
/*
 * 获取手机验证码
 */
namespace app\common\logic;

use think\Model;

use think\Session;

class Phone extends Model {

    private $phone = '';//手机号

    private $phoneFormat = '/^1\d{10}$/'; //手机号码正则表达式

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    //短信功能是否开启
    public function isMobileEnabled() {
        if (config('con_sms_status') == 0) {
            return false;
        }
        return true;
    }

    //手机号是否为空
    public function isPhoneEmpty() {
        if (empty($this->phone)) {
            return false;
        }
        return true;
    }

    //手机号格式是否正确
    public function isPhone() {
        if (preg_match($this->phoneFormat, $this->phone)) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否已经发送过验证码
     * param $code 对应的session码
     * param $limitTime 限制时间(单位：秒)
     */
    public function isSended($codeType, $timeType, $limitTime) {
        if (Session::get($codeType) != '') {
            if (Session::get($timeType) + $limitTime > time()) {
                return false;
            }
        }
        return true;
    }

    //获取发送验证码URL
    public function getSendUrl() {
        return config('con_sms_url');
    }

    //生成随机验证码
    private function createCode($len) {
        srand((double) microtime() * 1000000);
        $char = '0,1,2,3,4,5,6,7,8,9';
        $list = explode(",", $char);
        $code = '';
        for ($i = 0; $i < $len; $i++) {
            $randnum = rand(0, 9);
            $code .= $list[$randnum];
        }
        return $code;
    }

    //获取手机验证码
    public function getCode($data = []) {
        $this->setPhone($data['phone']);//手机号赋值
        $res = array('code' => 0);
        if (!$this->isMobileEnabled()) {
            $res['msg'] = '手机短信功能已关闭';
            return $res;
        }
        if (!$this->isPhoneEmpty()) {
            $res['msg'] = '手机号码不能为空';
            return $res;
        }
        if (!$this->isPhone()) {
            $res['msg'] = '请输入正确的手机号码';
            return $res;
        }
        //1分钟后可以继续发送验证码
        if (!$this->isSended('reg_sms_code', 'reg_sms_time', 60)) {
            $res['msg'] = '请1分钟后再试';
            return $res;
        }
        $smsUrl = $this->getSendUrl();
        if (empty($smsUrl)) {
            $res['msg'] = '请求失败,请联系客服';
            return $res;
        }
        $code = $this->createCode(6);
        $contents = '您的验证码是：' . $code . '。请不要把验证码泄露给其他人。';
        $smsUrl = str_replace("#", '', $smsUrl);
        $smsUrl = str_replace("phone", $this->phone, $smsUrl);
        $sendUrl = str_replace("content", $contents, $smsUrl);//请求的url
        $smsUrl = str_replace("content", urlencode($contents), $smsUrl);//存数据库的url
        $result = curl($sendUrl);
        if ($result) {
            session('reg_sms_code', $code);
            session('reg_sms_time', time());
            session('reg_phone', $this->phone);
            $sdata['user_id'] = 0;
            $sdata['type'] = 'reg';
            $sdata['phone'] = $this->phone;
            $sdata['contents'] = $contents;
            $sdata['send_return'] = $result;
            $sdata['send_url'] = $smsUrl;
            $sdata['code'] = $code;
            $id = model('approve_smslog')->add($sdata);
            return ['code' => 1, 'msg' => $id];
        } else {
            $res['msg'] = '请求失败,请稍后重试';
            return $res;
        }
    }

    public function checkCode($data = []) {
        $rs = model('approve_smslog')->getOne($data);
        return $rs ? true : false;
    }
}