<?php
namespace app\common\logic;

use think\Model;

class Coupon extends Model {
    public function sendCoupon($data) {
        if (!$data['user_id']) {
            return null;
        }
        $list = model('coupon')->getList([
                'type' => $data['type'] ?: 1,
                'status' => 1,
        ]);
        foreach ($list as $key => $val) {
            model('coupon')->send([
                    'type' => $data['type'],
                    'coupon_id' => $val['id'],
                    'user_id' => $data['user_id'],
                    'mark' => $data['mark']
            ]);
        }
        return true;
    }
}
