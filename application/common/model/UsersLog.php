<?php
namespace app\common\model;

use think\Model;

class UsersLog extends Model
{
    public function addUserLog($data = []) {
        if (!$data) {
            return null;
        }
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['client'] = 'app';
        $id = $this->insert($data);
        return $id;
    }
}
