<?php
namespace app\common\model;

use think\Model;

class User extends Model
{
    protected $name = 'users';
    public function getUser($where) {
        $user = $this->where($where)->find();
        return $user;
    }
    public function updateUserLogin($user_id) {
        if (!$user_id) {
            return false;
        }
        $ex = $this->where(['user_id' => $user_id])->update([
                'logintime' => ['exp', 'logintime+1'],
                'up_time' => ['exp', 'last_time'],
                'last_time' => time(),
                'up_ip' => ['exp', 'last_ip'],
                'last_ip' => get_client_ip()
        ]);
        return $ex;
    }
    public function addUser($data = []) {
        if (!$data['phone']) {
            return null;
        }
        $ip = get_client_ip();
        $insert = ['reg_time' => time(),
            'reg_ip' => $ip,
            'up_time' => time(),
            'up_ip' => $ip,
            'last_time' => time(),
            'last_ip' => $ip,
            'username' => $data['phone'],
            'phone' => $data['phone'],
            'password' => md5($data['phone']),
            'from' => 2
        ];
        $this->insert($insert);
        $user_id = $this->getLastInsID();
        return $user_id;
    }
    public function editPwd($user_id, $pwd) {
        return $this->where(['user_id' => $user_id])->update([
                'password' => $pwd
        ]);
    }
}
