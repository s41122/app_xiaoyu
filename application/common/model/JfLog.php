<?php
namespace app\common\model;

use think\Model;

class JfLog extends Model {
    public function add($data = []) {
        $data['addtime'] = time();
        return $this->insert($data);
    }
}
