<?php
namespace app\common\model;

use think\Model;

class ApproveSms extends Model
{
    public function add($data = [])
    {
        if (!$data || !$data['user_id'] || !$data['phone']) {
            return null;
        }
        $data['status'] = 1;
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['phone'] = $data['phone'];

        $re = $this->where(['user_id' => $data['user_id']])->find();
        if (!$re) {
            $id = $this->data($data)->insert($data);
            return $id;
        }
        return false;
    }
}