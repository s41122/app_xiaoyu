<?php
namespace app\common\model;

use think\Model;

class CouponUsers extends Model
{

    public function add($data = []) {
        $data['addtime'] = time();
        $data['modtime'] = time();
        $data['status'] = 1;
        $re = $this->data($data)->insert($data);
        return $re;
    }

}
