<?php
namespace app\common\model;

use think\Model;

class UsersInfo extends Model
{

    public function edit($data = []) {
        //判断是否存在
        $result = $this->where(['user_id' => $data['user_id']])->find();
        if (!$result) {
            $re = $this->insert($data);
        } else {
            $re = $this->where(['user_id' => $data['user_id']])->update($data);
        }
        return $re;
    }

}
