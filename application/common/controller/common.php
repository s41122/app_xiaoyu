<?php
namespace app\common\controller;

use think\Controller;

class Common extends Controller {

    public function _initialize() {


        $system = model('system')->getList();
        $system_config = array_column($system, 'value', 'nid');
        if ($system_config['con_webopen'] != 1) {
            die('网站已关闭!');
        }
        config($system_config);

        if (!user_ip_allow()) {
            die('您的IP不允许访问!');
        }
    }
}
