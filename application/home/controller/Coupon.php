<?php
/*
 * 优惠券
 */
namespace app\home\controller;

class Coupon extends Common {
    public function getList() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            $type = input('type', 0, 'intval');
            $status = input('status', 1, 'intval');//1可使用;2使用中;3已使用;0已过期
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'user_id不能为空']);
            }
            $list = model('coupon', 'logic')->getList([
                    'user_id' => $user_id,
                    'style' => $type,//0现金券1加息券
                    'status' => $status,
                    'limit' => 'all'
            ]);
            if (!$list) {
                $this->ajaxReturn(['code' => 0, 'msg' => '无记录']);
            }
            foreach ($list as $k => $v) {
                $return[$k]['id'] = $v['id'];
                $return[$k]['coupon_id'] = $v['coupon_id'];
                $return[$k]['money'] = $v['money'];
                $return[$k]['min'] = $v['min'];
                $return[$k]['min_day'] = $v['min_day'];
                $return[$k]['coupon_name'] = $v['coupon_name'];
                $return[$k]['max'] = $v['max'];
                $return[$k]['exptime'] = date('Y-m-d H:i:s', $v['exptime']);
                $return[$k]['interest'] = $v['interest'];
            }
            $this->ajaxReturn($return);
        }
    }
}
