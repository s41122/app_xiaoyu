<?php
/*
 * 登录
 */
namespace app\home\controller;

class Logout extends Common {
    public function index() {
        $model = model('user', 'logic');
        $model->logout();
        $this->redirect('/login');
    }
}