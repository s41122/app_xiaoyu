<?php
/*
 * 用户中心
 */
namespace app\home\controller;

use think\request;

class Account extends Common {
    public function getIndex(Request $request) {
        if ($request->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空']);
            }
            $borrow = model('borrow', 'logic')->GetUsersRecoverCount(['user_id' => $user_id]);
            $user = model('user')->getUser(['user_id' => $user_id]);
            $account = model('account')->getOne(['user_id' => $user_id]);
            $sign = model('sign')->getSign($user_id);

            require_once EXTEND_PATH . 'oss/samples/Common.php';
            \Common::createBucket('public-read');
            $bucket = \Common::getBucketName();
            $ossClient = \Common::getOssClient();
            if (!is_null($ossClient)) {
                if ($ossClient->doesObjectExist($bucket, "head/" . $user_id . '_avatar_middle.jpg')) {
                    $avatar = 'http://xiaoyu-admin.oss-cn-hangzhou.aliyuncs.com/head/' . $user_id . '_avatar_middle.jpg';
                } else {
                    $avatar = '';
                }
            }

            $data = [
                    'username' => $user['username'],
                    'total' => $account['total'],
                    'balance' => $account['balance'],
                    'wait' => $borrow['recover_wait_account'],
                    'profit' => $borrow['recover_yes_interest'],
                    'sign' => $sign ? 1 : 0,
                    'avatar' => $avatar
            ];
            $this->ajaxReturn($data);
        }
    }

    //修改登录密码
    public function editPwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'pwd' => input('pwd')
            ];
            $result = model('account', 'logic')->editPwd($data);
            $this->ajaxReturn($result);
        }
    }

    //设置支付密码
    public function setPaypwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'pwd' => input('pwd'),
                    'cpwd' => input('cpwd'),
                    'code' => input('code')
            ];
            $result = model('account', 'logic')->setPaypwd($data);
            $this->ajaxReturn($result);
        }
    }
    //设置支付密码
    public function paypwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'pwd' => input('pwd'),
                    'cpwd' => input('cpwd'),
                    'code' => input('code')
            ];
            $result = model('account', 'logic')->setPaypwd($data);
            $this->ajaxReturn($result);
        }
    }

    //判断验证码
    public function step1() {
        if (request()->isPost()) {
            $phone = input('phone');
            $code = input('code');
            $type = input('type');
            if (!isPhone($phone)) {
                $this->ajaxReturn(['code' => 0, 'msg' => '手机号码格式有误']);
            }
            if (!$code) {
                $this->ajaxReturn(['code' => 0, 'msg' => '手机验证码不能为空']);
            }
            if (!$type) {
                $this->ajaxReturn(['code' => 0, 'msg' => '类型不能为空']);
            }
            $valid = model('phone', 'logic')->checkCode([
                    'phone' => $phone,
                    'code' => $code,
                    'code_status' => 0,
                    'type' => $type
            ]);
            if ($valid) {
                model('phone', 'logic')->setCodeStatus([
                        'id' => $valid['id']
                ]);
                $this->ajaxReturn(['code' => 1, 'msg' => '验证码正确', 'tk' => md5($phone . $code)]);
            }
            $this->ajaxReturn(['code' => 0, 'msg' => '手机验证码错误']);
        }
    }

    //重置支付密码
    public function resetPaypwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'phone' => input('phone'),
                    'pwd' => input('pwd'),
                    'code' => input('code'),
                    'type' => input('type'),
                    'tk' => input('tk'),
            ];
            $result = model('account', 'logic')->resetPaypwd($data);
            $this->ajaxReturn($result);
        }
    }

    //重置登录密码
    public function resetPwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'phone' => input('phone'),
                    'pwd' => input('pwd'),
                    'code' => input('code'),
                    'type' => input('type'),
                    'tk' => input('tk'),
            ];
            $result = model('account', 'logic')->resetPwd($data);
            $this->ajaxReturn($result);
        }
    }

    //修改支付密码
    public function editPaypwd(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'pwd' => input('pwd'),
                    'oldpwd' => input('oldpwd'),
                    'cpwd' => input('cpwd'),
            ];
            $result = model('account', 'logic')->editPaypwd($data);
            $this->ajaxReturn($result);
        }
    }

    //日志
    public function getTradeLog() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }
            $page = input('page', 1, 'intval');
            $epage = input('epage', 10, 'intval');
            $list = model('account')->GetLogList([
                    'user_id' => $user_id,
                    'type' => input('type'),
                    'page' => $page,
                    'epage' => $epage,
            ]);
            if (!$list) {
                $this->ajaxReturn(false);
            }
            $type_list = model('linkages')->getNameList([
                    'nid' => 'account_type'
            ]);
            $typeArr = array_column($type_list, 'name', 'value');
            foreach ($list as $k => $v) {
                $return[$k]['time'] = date('Y-m-d H:i:s', $v['addtime']);
                $return[$k]['type'] = isset($typeArr[$v['type']]) ? $typeArr[$v['type']] : '';
                $return[$k]['money'] = $v['money'];
            }
            $this->ajaxReturn($return);
        }
    }

    //获取邀请码
    public function getInviteCode() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }
            $result = model('users_info')->getOne(['user_id' => $user_id]);
            if (!$result || !$result['invite_code']) {
                $this->ajaxReturn(false);
            }
            $this->ajaxReturn(['code' => 1, 'msg' => $result['invite_code']]);
        }
    }
}
