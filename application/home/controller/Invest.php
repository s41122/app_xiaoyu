<?php
/*
 * 投资
 */
namespace app\home\controller;

class Invest extends Common {
    //未回款
    public function getListWait() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }
            $result = model('borrow_tender')->GetTenderList([
                    'user_id' => $user_id,
                    'order' => 'order',
                    'status' => 0,
                    'limit' => 'all'
            ]);
            if (!$result) {
                $this->ajaxReturn(false);
            }
            $wait = model('borrow_tender')->wait([
                    'user_id' => $user_id
            ]);
            $return['total'] = $wait['total'];
            $return['account'] = $wait['account'];
            $return['interest'] = $wait['interest'];

            $list = [];
            foreach ($result as $k => $v) {
                $list[$k]['interest'] = $v['recover_account_interest'];
                $list[$k]['account'] = $v['account'];
                $list[$k]['name'] = $v['borrow_name'];
                $list[$k]['nid'] = $v['borrow_nid'];
                $list[$k]['id'] = $v['id'];
                $list[$k]['invest_time'] = date('Y-m-d H:i:s', $v['addtime']);
                $time = $v['borrow_type'] == 'day' ? strtotime('+ ' . $v['borrow_period'] . ' day ' . date('Y-m-d H:i:s', $v['reverify_time'])) : strtotime('+ ' . $v['borrow_period'] . ' month ' . date('Y-m-d H:i:s', $v['reverify_time']));
                $list[$k]['end_time'] = date('Y-m-d', $time);
            }
            $list = array_sort($list, 'end_time', 'asc');
            $return['list'] = $list;

            $this->ajaxReturn($return);
        }
    }

    //已回款
    public function getListDone() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }
            $result = model('borrow_tender')->GetTenderList([
                    'user_id' => $user_id,
                    'order' => 'order',
                    'status' => 1,
                    'limit' => 'all'
            ]);
            if (!$result) {
                $this->ajaxReturn(false);
            }
            $return = [];
            foreach ($result as $k => $v) {
                $return[$k]['interest'] = $v['recover_account_interest'];
                $return[$k]['account'] = $v['account'];
                $return[$k]['name'] = $v['borrow_name'];
                $return[$k]['nid'] = $v['borrow_nid'];
                $return[$k]['id'] = $v['id'];
                $return[$k]['tender_nid'] = $v['nid'];
                $return[$k]['invest_time'] = date('Y-m-d H:i:s', $v['addtime']);
                $time = $v['borrow_type'] == 'day' ? strtotime('+ ' . $v['borrow_period'] . ' day ' . date('Y-m-d H:i:s', $v['reverify_time'])) : strtotime('+ ' . $v['borrow_period'] . ' month ' . date('Y-m-d H:i:s', $v['verify_time']));
                $return[$k]['end_time'] = date('Y-m-d', $time);
            }
            $this->ajaxReturn($return);
        }
    }

    //投资详情
    public function getTenderDetail() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            $id = input('id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户id不能为空']);
            }
            if (!$id) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'id不能为空']);
            }
            $result = model('borrow_tender')->GetTenderOne([
                    'id' => $id,
                    'user_id' => $user_id
            ]);
            if (!$result) {
                $this->ajaxReturn(['code' => 0, 'msg' => '数据有误']);
            }
            $return = [
                    'name' => $result['name'],
                    'nid' => $result['borrow_nid'],
                    'account' => $result['account'],
                    'interst' => $result['recover_account_interest'],
                    'tender_time' => date('Y-m-d H:i', $result['addtime']),
                    'end_time' => $result['end_time'],
                    'style' => $result['style_title'],
                    'apr' => $result['borrow_apr'],
                    'apr_base' => $result['borrow_apr'] - $result['borrow_apr_extra'],
                    'apr_extra' => $result['borrow_apr_extra'],
            ];
            $coupon = model('coupon_users')->getOne([
                    'user_id' => $user_id,
                    'borrow_nid' => $result['borrow_nid']
            ]);
            if ($coupon) {
                $return['coupon_name'] = $coupon['style'] == 1 ? '加息券' : '红包';
                $return['money'] = $coupon['money'];
                $return['interest'] = $coupon['interest'];
            }

            $repay_list = model('borrow_tender')->GetRecoverList([
                    'user_id' => $user_id,
                    'tender_id' => $id,
                    'borrow_status' => 3,
                    'order' => 'repay_time'
            ]);
            if ($repay_list) {
                foreach ($repay_list as $key => $val) {
                    $repay[$key]['repay_time'] = $val['recover_time'];
                    $repay[$key]['account'] = $val['recover_capital'];
                    $repay[$key]['interest'] = $val['recover_interest'];
                    $repay[$key]['status'] = $val['recover_type_name'];
                }
                $return['repay_list'] = $repay;
            }

            $this->ajaxReturn($return);
        }
    }
}
