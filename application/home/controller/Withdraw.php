<?php
/*
 * 提现
 */
namespace app\home\controller;
use think\Config;

class Withdraw extends Common {
    public function info() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                return ['code' => 0, 'msg' => '用户ID不能为空'];
            }
            $account = model('account')->getOne(['user_id' => $user_id]);
            $user_bank = model('account_users_bank')->getOne(['user_id' => $user_id]);
            $bank_name = model('bank_card', 'logic')::getBankName($user_bank['bank_code']);
            $return = [
                    'bank_name' => $bank_name,
                    'bank_code' => substr($user_bank['account'], -4),
                    'bank_img' => model('bank_card', 'logic')::getBankImg($user_bank['bank_code']),
                    'balance_cash' => $account['balance_cash'],
                    'free_num' => $account['free_num'],
                    'frost_can' => $account['balance_frost_can'],
                    'img' => model('bank_card', 'logic')::getBankImg($user_bank['bank_code']),
            ];
            $this->ajaxReturn($return);
        }
    }

    public function addWithdraw() {
        if (request()->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'money' => input('money'),
                    'paypwd' => input('paypwd'),
            ];
            $return = model('withdraw', 'logic')->addWithdraw($data);
            $this->ajaxReturn($return);
        }
    }

    public function homeurl() {
        $data = http_build_query(input());
        $pay = Config::get('pay');
        $url = $pay['cashUrl'];
        model('fuyh5', 'logic')->postData($url, $data);
        $code = input('resp_code');
        if ($code == '0000'){
            return $this->fetch('homeurl');
        }
        return $this->fetch('reurl');
    }

    public function log() {
        $user_id = input('user_id');
        if (!$user_id) {
            $this->ajaxReturn(false);
        }
        $list = model('account_cash')->getList([
                'user_id' => $user_id
        ]);
        if (!$list) {
            $this->ajaxReturn(false);
        }
        $result = [];
        foreach ($list as $key => $val) {
            $result[$key]['money'] = $val['total'];
            $result[$key]['time'] = date('Y-m-d H:i:s', $val['addtime']);
            $result[$key]['status'] = $val['status'];
        }
        $this->ajaxReturn($result);
    }
}
