<?php
namespace app\home\controller;

class Discovery extends Common {
    //发现首页
    public function getIndex() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空']);
            }
            $userinfo = model('user')->getUserInfo($user_id, 'a.phone,a.paypassword,b.realname_status,c.bank_code,c.account');
            if (!$userinfo) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户不存在']);
            }
            $message = model('message')->getList(['limit' => 'all', 'user_id' => $user_id]);

            require_once EXTEND_PATH . 'oss/samples/Common.php';
            \Common::createBucket('public-read');
            $bucket = \Common::getBucketName();
            $ossClient = \Common::getOssClient();
            if (!is_null($ossClient)) {
                if ($ossClient->doesObjectExist($bucket, "head/" . $user_id . '_avatar_middle.jpg')) {
                    $avatar = 'https://xiaoyu-admin.oss-cn-hangzhou.aliyuncs.com/head/' . $user_id . '_avatar_middle.jpg';
                } else {
                    $avatar = '';
                }
            }

            $return = [
                    'auth_status' => $userinfo['realname_status'] ? 1 : 0,
                    'phone_hide' => hideMobile($userinfo['phone']),
                    'bank_name' => model('bank_card', 'logic')::getBankName($userinfo['bank_code']),
                    'last_number' => substr($userinfo['account'], -4),
                    'total' => isset($message['total']) ? $message['total'] : 0,
                    'noread' => isset($message['noread']) ? $message['noread'] : 0,
                    'paypwd' => $userinfo['paypassword'],
                    'phone' => $userinfo['phone'],
                    'avatar' => $avatar,
            ];
            $this->ajaxReturn($return);
        }
    }

    //我的消息
    public function getMessage() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }
            $result = model('message')->getList([
                    'user_id' => $user_id,
                    'page' => input('page'),
                    'epage' => input('epage')
            ]);
            if (!$result) {
                $this->ajaxReturn(false);
            }
            $return = [];
            foreach ($result as $key => $val) {
                $return[$key]['name'] = $val['name'];
                $return[$key]['contents'] = $val['contents'];
                $return[$key]['addtime'] = date('Y-m-d H:i:s', $val['addtime']);
            }
            $this->ajaxReturn($return);
        }
    }

    //客服页面获取用户信息
    public function getService() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'user_id不能为空']);
            }
            $user_info = model('user')->getUserInfo($user_id, 'b.realname,a.reg_time,a.last_time,b.sex,a.phone');
            if (!$user_info) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户信息不存在']);
            }

            $sex = model('linkages')->getName(['type' => 'rating_sex', 'nid' => $user_info['sex']]);
            $sex = $sex ?: '保密';
            $return = [
                    'real_name' => $user_info['realname'],
                    'reg_date' => date('Y-m-d H:i:s', $user_info['reg_time']),
                    'last_login' => date('Y-m-d H:i:s', $user_info['last_time']),
                    'sex' => $sex,
                    'phone' => $user_info['phone'],
            ];
            $this->ajaxReturn(['code' => 1, 'msg' => $return]);
        }
    }
}
