<?php
namespace app\home\controller;

use think\Request;

class Project extends Common {
    public function index() {
        $list = model('borrow', 'logic')->getList([
                'status_nid' => 'loan',
        ]);
        $this->assign('list', $list);
        return $this->fetch();
    }

    //获取项目首页数据
    public function getIndex(Request $request) {
        if ($request->isPost()) {
            $list = model('borrow', 'logic')->getList([
                    'status_nid' => 'loan'
            ]);
            if (!$list) {
                $result = false;
            } else {
                foreach ($list as $k => $v) {
                    $result[$k]['id'] = $v['id'];
                    $result[$k]['name'] = $v['name'];
                    $result[$k]['nid'] = $v['borrow_nid'];
                    $result[$k]['apr'] = $v['borrow_apr'];
                    $result[$k]['apr1'] = $v['borrow_apr1'];
                    $result[$k]['apr_extra'] = $v['borrow_apr_extra'];
                    $result[$k]['period'] = $v['borrow_period_name'];
                    $result[$k]['tiro'] = $v['tiro_status'] ? 1 : 0;
                    $result[$k]['period'] = $v['borrow_period_name'];
                    $result[$k]['wait'] = $v['borrow_account_wait'];
                    $result[$k]['scale'] = $v['borrow_account_scale'];
                    $result[$k]['begin_time'] = $v['begin_time'];
                }
            }

            $list_full = model('borrow', 'logic')->getList([
                    'status_nid' => 'full_or_repay',
                    'limit' => 10
            ]);
            if (!$list_full) {
                $result_full = false;
            } else {
                foreach ($list_full as $k => $v) {
                    $result_full[$k]['id'] = $v['id'];
                    $result_full[$k]['name'] = $v['name'];
                    $result_full[$k]['nid'] = $v['borrow_nid'];
                    $result_full[$k]['apr'] = $v['borrow_apr'];
                    $result_full[$k]['apr1'] = $v['borrow_apr1'];
                    $result_full[$k]['apr_extra'] = $v['borrow_apr_extra'];
                    $result_full[$k]['period'] = $v['borrow_period_name'];
                    $result_full[$k]['tiro'] = $v['tiro_status'] ? 1 : 0;
                    $result_full[$k]['period'] = $v['borrow_period_name'];
                    $result_full[$k]['wait'] = $v['borrow_account_wait'];
                    $result_full[$k]['scale'] = $v['borrow_account_scale'];
                    $result_full[$k]['begin_time'] = $v['begin_time'];
                }
            }

            $arr = [
                    'list' => $result,
                    'list_full' => $result_full
            ];
            $this->ajaxReturn($arr);
        }
    }

    public function getList(Request $request) {
        if ($request->isPost()) {
            $list = model('borrow', 'logic')->getList([
                    'status_nid' => 'invest',
                    'query_type' => 'invest'
            ]);
            return $list;
        }
    }

    //获取项目详情-投资列表
    public function getTenderList() {
        $borrow_nid = input('nid');
        if (!$borrow_nid) {
            $this->ajaxReturn(false);
        }
        $list = model('borrow_tender')->GetTenderList(['borrow_nid' => $borrow_nid, 'limit' => 'all']);
        if (!$list) {
            $this->ajaxReturn(false);
        }
        foreach ($list as $k => $v) {
            $return[$k]['phone'] = hideMobile($v['username']);
            $return[$k]['account'] = $v['account'];
            $return[$k]['time'] = date('Y-m-d H:i:s', $v['addtime']);
        }
        $this->ajaxReturn($return);
    }

    //获取项目详情数据
    public function getDetail(Request $request) {
        if ($request->isPost()) {
            $borrow_nid = input('nid');
            if (!$borrow_nid) {
                $this->ajaxReturn(false);
            }

            $detail = model('borrow', 'logic')->getDetail([
                    'borrow_nid' => $borrow_nid,
                    'hits' => 'auto'
            ]);

            if (!$detail) {
                $this->ajaxReturn(false);
            }

            $tender_count = model('borrow_tender')->GetTenderCount(['borrow_nid' => $detail['borrow_nid']]);

            $return = [
                    'apr' => $detail['borrow_apr'],
                    'apr_base' => $detail['borrow_apr1'],
                    'apr_extra' => $detail['borrow_apr_extra'],
                    'period' => $detail['borrow_period_name'],
                    'min' => $detail['tender_account_min'],
                    'wait' => $detail['borrow_account_wait'],
                    'scale' => $detail['borrow_account_scale'],
                    'account' => $detail['account'],
                    'name' => $detail['name'],
                    'nid' => $detail['borrow_nid'],
                    'style_title' => $detail['style_title'],
                    'tender_people' => $tender_count ? $tender_count['total_people'] : 0,
                    'tender_count' => $tender_count ? $tender_count['total_count'] : 0,
                    'status' => $detail['status'],
                    'repay_wait' => $detail['repay_account_wait'],
                    'begin_time' => $detail['begin_time'],
                    'status_name' => $detail['status_name'],
            ];
            $this->ajaxReturn($return);
        }
    }

    //获取立即投资页面数据
    public function tenderDetail(Request $request) {
        if ($request->isPost()) {
            $borrow_nid = input('nid');
            if (!$borrow_nid) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'nid不能为空']);
            }
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'user_id不能为空']);
            }
            $detail = model('borrow', 'logic')->getDetail([
                    'borrow_nid' => $borrow_nid,
            ]);

            $coupons_count = model('coupon', 'logic')->getCount([
                    'user_id' => $user_id,
                    'style' => 1,//0现金券1加息券
                    'status' => 0
            ]);
            $red_count = model('coupon', 'logic')->getCount([
                    'user_id' => $user_id,
                    'style' => 0,//0现金券1加息券
                    'status' => 0
            ]);
            $account = model('account')->getOne(['user_id' => $user_id]);
            $return = [
                    'name' => $detail['name'],
                    'nid' => $detail['borrow_nid'],
                    'wait' => $detail['borrow_account_wait'],
                    'balance' => $account['balance_cash'] + $account['balance_frost'],
                    'coupon' => $coupons_count,
                    'red' => $red_count,
                    'account' => $detail['account'],
                    'period' => $detail['borrow_period'],
                    'type' => $detail['borrow_type']
            ];
            $this->ajaxReturn($return);
        }
    }

    //立即投资
    public function addTender(Request $request) {
        if ($request->isPost()) {
            $data = [
                    "borrow_nid" => input('borrow_nid'),
                    "user_id" => input('user_id'),
                    "coupon_users_id" => input('coupon_users_id'),
                    'account' => input('account'),
                    'paypassword' => input('paypwd'),
            ];
            $result = model('borrow_tender', 'logic')->addTender($data);
            $this->ajaxReturn($result);
        }
    }

    public function getCouponList() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            $type = input('type', 0, 'intval');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => 'user_id不能为空']);
            }
            $list = model('coupon', 'logic')->getList([
                    'user_id' => $user_id,
                    'style' => $type,//0现金券1加息券
                    'status' => 0,
                    'limit' => 'all'
            ]);
            if (!$list) {
                $this->ajaxReturn(['code' => 0, 'msg' => '无记录']);
            }
            foreach ($list as $k => $v) {
                $return[$k]['id'] = $v['id'];
                $return[$k]['coupon_id'] = $v['coupon_id'];
                $return[$k]['money'] = $v['money'];
                $return[$k]['min'] = $v['min'];
                $return[$k]['min_day'] = $v['min_day'];
                $return[$k]['coupon_name'] = $v['coupon_name'];
                $return[$k]['max'] = $v['max'];
                $return[$k]['exptime'] = date('Y-m-d H:i:s', $v['exptime']);
                $return[$k]['interest'] = $v['interest'];
            }
            $this->ajaxReturn($return);
        }
    }
}
