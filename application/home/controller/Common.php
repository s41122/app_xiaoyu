<?php
namespace app\home\controller;

use think\Controller;

class Common extends Controller {
    public function _initialize() {
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Methods: GET, POST, DELETE");
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
        $system = model('system')->getList();
        foreach ($system as $k => $v) {
            $system_config[$v['nid']] = $v['value'];
        }
        config($system_config);
    }
}
