<?php
namespace app\home\controller;

use think\Request;

class Phone extends Common {
    public function getCode() {
        $request = Request::instance();
        if ($request->isPost()) {
            $phone = input('phone');
            $type = input('type');
            $limit = input('limit', 60);
            $user_id = input('user_id', 0, 'intval');
            $res = model('phone', 'logic')->getCode([
                    'phone' => $phone,
                    'type' => $type,
                    'limit' => $limit,
                    'user_id' => $user_id
            ]);
            if ($res['code'] == 1) {
                $this->ajaxReturn(['code' => 1, 'msg' => '获取验证码成功']);
            } else {
                $this->ajaxReturn($res);
            }
        }
    }

    //判断验证码是否正确
    public function checkCode() {
        if (request()->isPost()) {
            $phone = input('phone');
            $code = input('code');
            $type = input('type');
            if (!isPhone($phone)) {
                $this->ajaxReturn(['code' => 0, 'msg' => '手机号码格式有误']);
            }
            if (!$code) {
                $this->ajaxReturn(['code' => 0, 'msg' => '手机验证码不能为空']);
            }
            if (!$type) {
                $this->ajaxReturn(['code' => 0, 'msg' => '类型不能为空']);
            }
            $valid = model('phone', 'logic')->checkCode([
                    'phone' => $phone,
                    'code' => $code,
                    'code_status' => 0,
                    'type' => $type
            ]);
            if ($valid) {
                model('phone', 'logic')->setCodeStatus([
                        'id' => $valid['id']
                ]);
                $this->ajaxReturn(['code' => 1, 'msg' => '验证码正确']);
            }
            $this->ajaxReturn(['code' => 0, 'msg' => '验证码错误']);
        }
    }
}
