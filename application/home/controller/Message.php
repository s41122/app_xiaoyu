<?php
namespace app\home\controller;

class Message extends Common {
    public function index() {
        return $this->fetch();
    }

    public function updateStatus() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空!']);
            }
            db('message_receive')->where(['user_id' => $user_id])->update(['status' => 1]);
            $this->ajaxReturn(['code' => 1, 'msg' => 'success']);
        }
    }
}
