<?php
/*
 * 登录
 */
namespace app\home\controller;

class Login extends Common {
    public function login() {
        if (request()->isPost()) {
            $phone = input('phone');
            $pwd = input('pwd');

            $result = model('user', 'logic')->login($phone, $pwd);
            if ($result['code'] != 1) {
                $this->ajaxReturn(['code' => 0, 'msg' => $result['msg'], 'err_num' => session('login_error_number_' . $phone)]);
            } else {
                $this->ajaxReturn(['code' => 1, 'msg' => '登录成功', 'user_id' => $result['uid']]);
            }
        }
    }

    public function index() {
        return $this->fetch();
    }

    public function index1() {
        $isReg = 0;
        $phone = input('phone');
        if (!$phone) {
            $this->redirect('index');
        }
        $user = model('user')->getUser(['username' => $phone]);
        if ($user) {
            $isReg = 1;
        }
        $this->assign('isReg', $isReg);
        return $this->fetch();
    }

    //注册协议
    public function zcxy() {
        return $this->fetch();
    }
}
