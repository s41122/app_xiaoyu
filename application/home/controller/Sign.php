<?php
/*
 * 签到
 */
namespace app\home\controller;

use think\Controller;

class Sign extends Controller {
    public function sign() {
        if (request()->isPost()) {
            $result = model('sign', 'logic')->sign([
                    'user_id' => input('user_id'),
            ]);
            $this->ajaxReturn($result);
        }
    }
}
