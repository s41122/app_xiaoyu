<?php
namespace app\home\controller;

class Charge extends Common {
    public function charge() {
        model('baopay', 'logic')->test([
                'money' => 2
        ]);
        exit;
    }

    public function getInfo() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空']);
            }
            $user_bank = model('account_users_bank')->getOne(['user_id' => $user_id]);

            $plist = model('account_payment')->getList([
                    'status' => 1,
                    'from' => 2
            ]);
            if (!$plist) {
                $this->ajaxReturn(['code' => 0, 'msg' => '没有可用的支付方式!']);
            }
            $return = [
                    'name' => model('bank_card', 'logic')::getBankName($user_bank['bank_code']),
                    'last' => substr($user_bank['account'], -4),
                    'img' => model('bank_card', 'logic')::getBankImg($user_bank['bank_code']),
            ];
            foreach ($plist as $k => $v) {
                $paylist[$k]['id'] = $v['id'];
                $paylist[$k]['name'] = $v['name'];
            }
            $return['list'] = $paylist;
            $this->ajaxReturn($return);
        }
    }

    public function getCharge() {

        if (request()->isPost()) {
            $money = input('money');
            $result = model('charge', 'logic')->charge([
                    'payment' => input('pid'),
                    'user_id' => input('user_id'),
                    'money' => $money
            ]);

            $this->ajaxReturn($result);
        }
    }

    public function callback() {
        $payment = input('payment');
        if (!$payment) {
            $this->error('非法访问!');
        }
        model('charge', 'logic')->callback(input());
    }

    /**
     * @return array
     */
    public function getLog() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空!']);
            }
            $list = model('account_recharge')->getList([
                    'a.user_id' => $user_id
            ]);
            if (!$list) {
                $this->ajaxReturn(['code' => 0, 'msg' => '无充值记录!']);
            }
            $result = [];
            foreach ($list as $key => $val) {
                $result[$key]['money'] = $val['money'];
                $result[$key]['payment'] = $val['name'];
                $result[$key]['time'] = date('Y-m-d H:i:s', $val['addtime']);
                $result[$key]['status'] = $val['status'];
            }
            $this->ajaxReturn($result);
        }
    }

    public function homeurl() {
        $result = model('fuyh5', 'logic')->callback(input());
        $code = input('resp_code');
        if ($code == '0000' && $result == true){
            return $this->fetch('homeurl');
        }
        return $this->fetch('reurl');
    }

//    public function reurl() {
//        return $this->fetch();
//    }

    //宝付绑定四要素
    public function baofuBind() {
        if (request()->isPost()) {
            $user_id = input('user_id', 0, 'intval');
            if (!$user_id) {
                $this->ajaxReturn(false);
            }

            $result = model('account_payment')->getOne(['nid' => 'baofuh5']);
            if (!$result || $result['status'] != 1) {
                $this->ajaxReturn(false);
            }
            if ($result['status'] == 1) {
                $bind_result = model('baofu_bind')->getOne(['user_id' => $user_id, 'status' => 1]);
                if ($bind_result) {
                    $this->ajaxReturn(false);
                }

                $payclass = new \baofu\Baofu();
                $user_bank = model('account_users_bank')->getOne(['user_id' => $user_id]);
                if ($user_bank) {
                    $bresult = $payclass->ToSubmit([
                            'acc_no' => $user_bank['account'],
                            'txn_sub_type' => '01',
                            'id_card' => $user_bank['id_card'],
                            'id_holder' => $user_bank['name'],
                            'mobile' => $user_bank['phone'],
                            'pay_code' => $user_bank['bank_code'],
                    ]);
                    if ($bresult['status'] == 1) {
                        model('baofu_bind')->add([
                                'user_id' => $user_id,
                                'bind_id' => $bresult['return_data']['bind_id'],
                                'trans_id' => $bresult['return_data']['trans_id'],
                                'trade_date' => $bresult['return_data']['trade_date'],
                                'trans_serial_no' => $bresult['return_data']['trans_serial_no'],
                        ]);
                        $this->ajaxReturn(true);
                    }
                }
            }
            $this->ajaxReturn(false);
        }
    }

    //宝付预支付(获取验证码)
    public function baofuGetcode() {
        if (request()->isPost()) {
            $money = input('money');
            if (!is_numeric($money) || $money < 0.01) {
                $this->ajaxReturn(['code' => 0, 'msg' => '充值金额不能小于1分钱']);
            }
            $user_id = input('user_id', 0, 'intval');
            if (!$user_id) {
                $this->ajaxReturn(['code' => 0, 'msg' => '用户ID不能为空']);
            }
            if ((session('baofu_smscode_time') + 60) > time() && session('baofu_smscode_user_id') == $user_id) {
                $this->ajaxReturn(['status' => 0, 'msg' => '请过1分钟后再试']);
            }

            $baofu_bind = model('baofu_bind')->getOne(['user_id' => $user_id, 'status' => 1]);
            $bind_id = $baofu_bind['bind_id'];
            if (!isset($baofu_bind['bind_id']) || $baofu_bind['bind_id'] == '') {
                $this->ajaxReturn(['code' => 0, 'msg' => '您还未进行宝付四要素认证!']);
            }

            db()->startTrans();

            $recharge_data['type'] = 1;
            $recharge_data['money'] = $money;
            $recharge_data['payment'] = 48;
            $recharge_data['user_id'] = $user_id;
            $recharge_data['status'] = 0;
            $recharge_data['remark'] = "在线充值(baofoo)";
            $recharge_data['nid'] = $user_id . time() . rand(1000, 9999);//订单号;
            $charge_id = model('account_recharge')->add($recharge_data); //添加充值订单，返回ID号
            if (!$charge_id) {
                db()->rollback();
                $this->ajaxReturn(['code' => 0, 'msg' => '添加充值订单失败,请重试!']);
            }

            $payclass = new \baofu\Baofu();
            $payment_result = $payclass->ToSubmit([
                    'txn_amt' => $money,
                    'txn_sub_type' => 15,
                    'bind_id' => $bind_id,
                    'trans_id' => $recharge_data['nid']
            ]);
            if ($payment_result['status'] == 1) {
                $ex = model('account_recharge')->edit(['nid' => $recharge_data['nid']], ['business_no' => $payment_result['return_data']['business_no']]);
                if (!$ex) {
                    db()->rollback();
                }

                session('baofu_smscode_time', time());
                session('baofu_smscode_user_id', time());

                db()->commit();
                $this->ajaxReturn(['code' => 1, 'msg' => '验证码已发送到您的银行预留手机号中', 'business_no' => $payment_result['return_data']['business_no']]);
            } else {
                db()->rollback();
                $this->ajaxReturn(['code' => 0, 'msg' => '获取验证码失败!' . $payment_result['msg']]);
            }
        }
    }

    //宝付确认支付
    public function baofuConfirmCharge() {
        if (request()->isPost()) {
            $user_id = input('user_id');
            $business_no = input('business_no');
            $code = input('code');
            if (!$business_no || !$user_id || !$code) {
                $this->ajaxReturn(['code' => 0, 'msg' => '参数错误!']);
            }

            $result = model('account_recharge')->getOne(['business_no' => $business_no, 'user_id' => $user_id, 'status' => 0]);
            if (!$result) {
                $this->ajaxReturn(['code' => 0, 'msg' => '充值订单数据或状态有误!']);
            }

            db()->startTrans();

            $payclass = new \baofu\Baofu();
            $payment_result = $payclass->ToSubmit([
                    'txn_sub_type' => 16,
                    'code' => $code,
                    'trans_id' => $result['nid'],
                    'business_no' => $business_no
            ]);
            if ($payment_result['status'] == 1) {
                $re = model('charge', 'logic')->OnlineReturn(['trade_no' => $result['nid']]);
                if (!$re) {
                    db()->rollback();
                    $this->ajaxReturn(['code' => 0, 'msg' => '充值失败!']);
                }
                db()->commit();
                $this->ajaxReturn(['code' => 1, 'msg' => '充值成功!']);
            } else {
                $re = model('charge', 'logic')->OnlineReturnNo(['trade_no' => $result['nid'], 'verify_remark' => $payment_result['msg']]);
                if (!$re) {
                    db()->rollback();
                    $this->ajaxReturn(['code' => 0, 'msg' => '充值失败!']);
                }
                db()->commit();
                $this->ajaxReturn(['code' => 0, 'msg' => '充值失败!' . $payment_result['msg']]);
            }
        }
    }

    //宝付绑定四要素测试
    public function baofuBindTest() {
        if (request()->isPost()) {
            $payclass = new \baofu\Baofu();
            $user_bank = [
                    'account' => '6217920170427608',
                    'id_card' => '310107199607051714',
                    'name' => '徐新源',
                    'phone' => '15901878992',
                    'bank_code' => 'SPDB'
            ];
            if ($user_bank) {
                $bresult = $payclass->ToSubmit([
                        'acc_no' => $user_bank['account'],
                        'txn_sub_type' => '01',
                        'id_card' => $user_bank['id_card'],
                        'id_holder' => $user_bank['name'],
                        'mobile' => $user_bank['phone'],
                        'pay_code' => $user_bank['bank_code'],
                ]);
            }
            $this->ajaxReturn($bresult);
        }
    }
}
