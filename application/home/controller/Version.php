<?php
namespace app\home\controller;

class Version extends Common {
    public function getVersion() {
        if (request()->isPost()) {
            $system = input('system', 1, 'intval');
            $status = input('status', 0, 'intval');
            $v = model('version')->getOne(['system' => $system, 'status' => $status]);
            if (!$v) {
                $this->ajaxReturn(false);
            }
            $v['createtime'] = date('Y-m-d H:i:s', $v['createtime']);
            $this->ajaxReturn(['code' => 0, 'msg' => $v]);
        }
    }
}
