<?php
/*
 * 首页
 */
namespace app\home\controller;

class Index extends Common {
    public function index() {
        $borrow_new = model('borrow', 'logic')->getNewList();
        $borrow = model('borrow', 'logic')->getBorrowList();
        $result = [
                'new' => $borrow_new,
                'list' => $borrow
        ];

        if (request()->isPost()) {
            $this->ajaxReturn($result);
        }
        $this->assign('borrow', $result);
        return $this->fetch();
    }

    public function getIp() {
        $this->ajaxReturn(get_client_ip());
    }

    public function getBanner() {
        $typename = input('typename', 'app');
        $result = model('scrollpic')->getList([
                'a.status' => 1,
                'b.typename' => $typename
        ], ['a.url, a.name, a.pic, a.addtime']);
        if (!$result) {
            $this->ajaxReturn(false);
        } else {
            $this->ajaxReturn(['code' => 1, 'data' => $result]);
        }

    }

    public function getSevenAccount()
    {
        $result = model('borrow', 'logic')->getSevenAccount();
        if (!$result) {
            $this->ajaxReturn(false);
        } else {
            $this->ajaxReturn(['code' => 1, 'data' => $result]);
        }
    }
}
