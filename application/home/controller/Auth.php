<?php
/*
 * 实名认证
 */
namespace app\home\controller;

use think\Request;

class Auth extends Common {
    public function index() {
        $user_id = isLogin();
        if (!$user_id) {
            $this->redirect('/login');
        }
        return $this->fetch();
    }

    public function auth(Request $request) {
        if ($request->isPost()) {
            $data = [
                    'user_id' => input('user_id'),
                    'realname' => input('realname'),
                    'card_id' => input('card_id'),
                    'account' => input('account'),
//                    'phone' => input('phone'),
                    'code' => input('code'),
            ];
            $result = model('auth', 'logic')->auth($data);
            return $this->ajaxReturn($result);
        }
    }

    public function test() {
        //对接第三方实名认证接口
        $i_data['name'] = '徐汉才';
        $i_data['cardNo'] = '6212263602096437975';
        $i_data['certNo'] = '441581199107197798';
        $i_data['phoneNo'] = '13719554787';

//        $i_data['memberid'] = 'P204Y310';
//        $i_data['memberkey'] = 'ym123456';
//        $i_data['timeStamp'] = time() * 1000;

        $sign_str = $i_data['memberid'] . '|' . $i_data['memberkey'] . '|' . $i_data['name'] . '|' . $i_data['certNo'] . '|' . $i_data['cardNo'] . '|' . $i_data['timeStamp'] . '|C3t0pfJIrvKLzWBy';
        $i_data['sign'] = md5($sign_str);

        $i_data['name'] = urlencode($i_data['name']);
        $postFields = http_build_query($i_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, 'http://jk.paocaikj.com/paocai/queryBankCardVerification');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $result = curl_exec($ch);

        curl_close($ch);
        $this->ajaxReturn($result);
    }
}
