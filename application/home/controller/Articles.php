<?php
namespace app\home\controller;

class Articles extends Common {

    //文章列表
    public function getList() {
        $type_id = input('type_id');
        if (!$type_id) {
            $this->ajaxReturn(false);
        }
        $result = model('articles')->getList(['type_id' => $type_id, 'status' => 1, 'page' => input('page'), 'epage' => input('epage')]);
        if (!$result) {
            $this->ajaxReturn(false);
        } else {
            $return = [];
            foreach ($result as $key => $val) {
                $return[$key]['id'] = $val['id'];
                $return[$key]['publish'] = $val['publish'];
                $return[$key]['contents'] = $val['contents'];
                $return[$key]['name'] = $val['name'];
            }
            $this->ajaxReturn($return);
        }
    }

    //文章详情
    public function getDetail() {
        $id = input('id');
        if (!$id) {
            $this->ajaxReturn(false);
        }
        $result = model('articles')->getDetail(['id' => $id, 'hits_status' => 1]);
        if (!$result) {
            $this->ajaxReturn(false);
        } else {
            $return = [
                    'publish' => $result['publish'],
                    'contents' => $result['contents'],
                    'name' => $result['name'],
            ];
            $this->ajaxReturn($return);
        }
    }

    //文章列表
    public function getTypeList() {
        $pid = input('pid');
        if (!$pid) {
            $this->ajaxReturn(false);
        }
        $result = model('articles')->getTypeList(['pid' => $pid, 'limit' => 'all']);
        if (!$result) {
            $this->ajaxReturn(false);
        } else {
            $return = [];
            foreach ($result as $key => $val) {
                $return[$key]['id'] = $val['id'];
                $return[$key]['pid'] = $val['pid'];
                $return[$key]['name'] = $val['name'];
            }
            $this->ajaxReturn($return);
        }
    }
}
