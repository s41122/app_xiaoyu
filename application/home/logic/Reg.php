<?php
namespace app\home\logic;

use think\Model;

class Reg extends Model {

    public function reg($data = []) {
        if (!$data['phone'] || !isPhone($data['phone'])) {
            return ['code' => 0, 'msg' => '请输入正确的手机号码'];
        }
        if (!$data['code']) {
            return ['code' => 0, 'msg' => '请输入验证码'];
        }
        //注册判断是否是被屏蔽的ip
        if (!user_ip_allow()) {
            return ['code' => 0, 'msg' => '你所在IP不允许注册'];
        }

        //验证手机验证码
        $model_phone = model('phone', 'logic');
        if (!$model_phone->checkCode(['phone' => $data['phone'], 'code' => $data['code']])) {
            return ['code' => 0, 'msg' => '验证码错误'];
        }

        $check = model('user')->getUser(['username' => $data['phone']]);
        if ($check) {
            return ['code' => 0, 'msg' => '手机号已被注册'];
        }

        //添加用户
        $user_id = model('user')->addUser(['phone' => $data['phone'], 'pwd' => $data['pwd']]);
        //添加用户信息

        if ($user_id) {
            $unique = model('unique', 'logic');
            $invite_code = $unique::get($user_id, 6);

            model('users_info')->edit([
                    'user_id' => $user_id,
                    'phone' => $data['phone'],
                    'status' => 1,
                    'invite_code' => $invite_code,
            ]);

            //邀请人
            if ($data['inviter']) {//手机号或邀请码
                //获取邀请人
                $inviter_uid = model('user')->getInviter($data['inviter']);
                if ($inviter_uid) {
                    model('users_friends')->addFriends($user_id, $inviter_uid);
                    model('users_friends_invite')->addFriendsInvite($user_id, $inviter_uid);
                    model('spread', 'logic')->addSpread([
                            'spreads_userid' => $inviter_uid,
                            'user_id' => $user_id,
                            'type' => 'reg'
                    ]);
                }
            }

            //注册送红包
            model('coupon', 'logic')->sendCoupon([
                    'user_id' => $user_id,
                    'type' => 1,
                    'status' => 1,
                    'mark' => '注册送红包',
            ]);

            //注册送积分
            model('account', 'logic')->sendScore([
                    'user_id' => $user_id,
                    'nid' => 'reg',
                    'beizhu' => '注册所得积分'
            ]);
            //加入cookie
            $_cookie['user_id'] = $user_id;
            $_cookie['cookie_status'] = config('con_cache_type');
            $_cookie["cookie_id"] = config('con_cookie_id');
            $_cookie['time'] = 7 * 60 * 60 * 24;
            SetCookies($_cookie);

            return ['code' => 1, 'msg' => '注册成功!', 'user_id' => $user_id];
        } else {
            return ['code' => 0, 'msg' => '注册失败,请重试!'];
        }
    }

    public function second($data = []) {
        if (!$data['user_id']) {
            return ['code' => 0, 'msg' => '请先注册'];
        }
        if (strlen($data['pwd']) < 6 || strlen($data['pwd']) > 15) {
            return ['code' => 0, 'msg' => '密码必须6-15位'];
        }
        if ($data['pwd'] != $data['cpwd']) {
            return ['code' => 0, 'msg' => '密码不一致'];
        }
        $modelUser = model('user');
        $user = $modelUser->getUser(['user_id' => $data['user_id']]);
        if (!$user) {
            return ['code' => 0, 'msg' => '请先注册!'];
        }
        if ($user['password'] != '') {
            return ['code' => 0, 'msg' => '您已修改密码!'];
        }
        $data['password'] = md5($data['pwd']);
        $modelUser->editPwd($data['user_id'], md5($data['pwd']));

        //邀请人
        if ($data['inviter']) {
            //获取邀请人
            $inviter_uid = $modelUser->getInviter($data['inviter']);
            if ($inviter_uid) {
                model('users_friends')->addFriends($data['user_id'], $inviter_uid);
                model('users_friends_invite')->addFriendsInvite($data['user_id'], $inviter_uid);
                model('spread', 'logic')->addSpread([
                        'spreads_userid' => $inviter_uid,
                        'user_id' => $data['user_id'],
                        'type' => 'reg'
                ]);
            }
        }
        return ['code' => 1, 'msg' => '注册成功'];
    }

}