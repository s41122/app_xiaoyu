<?php
namespace app\home\logic;

use think\Model;

class Identify extends Model {
    protected $memberid, $memberkey, $key, $url;
    public function initialize() {
        parent::initialize();
//        $this->memberid = 'P204Y310';
//        $this->memberkey = 'ym123456';//商户秘钥
//        $this->key = 'C3t0pfJIrvKLzWBy';//签名秘钥
//        $this->url = 'http://jk.paocaikj.com/paocai/queryBankCardVerification';
    }

    public function check($data = []) {
        //真实姓名
        if ($data['name'] == '') {
            return null;
        }
        //真实姓名
        if ($data['phoneNo'] == '') {
            return null;
        }
        //身份证号
        if ($data['certNo'] == '') {
            return null;
        }
        //银行卡号
        if ($data['cardNo'] == '') {
            return null;
        }

        $num = $this->num($data['user_id']);
        if ($num >= 3) {
            return ['status' => 0, 'msg' => '认证次数已超过3次,无法认证!'];
        }

        $i_data['memberid'] = $this->memberid;
        $i_data['memberkey'] = $this->memberkey;
        $i_data['name'] = $data['name'];
        $i_data['certNo'] = $data['certNo'];
        $i_data['cardNo'] = $data['cardNo'];
        $i_data['phoneNo'] = $data['phoneNo'];
        $i_data['timeStamp'] = time() * 1000;

        $sign_str = $this->memberid . '|' . $this->memberkey . '|' . $i_data['name'] . '|' . $i_data['certNo'] . '|' . $i_data['cardNo'] . '|' . $i_data['timeStamp'] . '|' . $this->key;
        $i_data['sign'] = md5($sign_str);

        $i_data['name'] = urlencode($i_data['name']);
        $postFields = http_build_query($i_data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $result = curl_exec($ch);

        curl_close($ch);
        $re = $this->analysis($result);

        $i_data['user_id'] = $data['user_id'];
        $i_data['name'] = $data['name'];
        $this->log($i_data, $result);

        return $re;
    }

    function analysis($result = '') {
        $return = ['status' => 1, 'msg' => '认证成功'];
        if (!$result) {
            $return = ['status' => 0, 'msg' => '认证失败!'];
        } else {
            $result = json_decode($result, true);
            if (!isset($result['resultInfo']['data']['detailRespCode']) || $result['resultInfo']['data']['detailRespCode'] != '0000') {
                $return = ['status' => 0, 'msg' => ($result['resultInfo']['data']['detailRespMsg'] ?: '认证失败')];
            }
        }
        return $return;
    }

    function log($data, $result = '') {
        $re = json_decode($result, true);
        $data = [
                'param' => serialize($data),
                'result' => $result,
                'createtime' => time(),
                'ip' => get_client_ip(),
                'user_id' => $data['user_id'],
                'name' => $data['name'],
                'cardNo' => $data['cardNo'],
                'certNo' => $data['certNo'],
                'phoneNo' => $data['phoneNo'],
                'status' => $re['resultInfo']['data']['detailRespCode'] == '0000' ? 1 : 2
        ];
        db('approve_realname_log')->insert($data);
    }

    function num($user_id) {
        if (!$user_id) {
            return null;
        }
        $result = db('approve_realname_log')->where(['user_id' => $user_id])->count();
        return $result ?: 0;
    }
}
