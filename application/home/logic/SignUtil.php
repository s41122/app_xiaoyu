<?php
namespace app\home\logic;
//FROM FUIOU_LEO
class SignUtil {
	 /** 
     * RSA加签 
     * @param $paramStr 
     * @param $priKey 
     * @return string 
     */  
    public static function sign($paramStr, $priKey){  
        $sign = '';  
        //将字符串格式公私钥转为pem格式公私钥  
        $priKeyPem = SignUtil::format_secret_key($priKey, 'pri');
         
        //转换为openssl密钥，必须是没有经过pkcs8转换的私钥  
        $res = openssl_get_privatekey($priKeyPem);  
        //调用openssl内置签名方法，生成签名$sign  
        openssl_sign($paramStr, $sign, $res);
        //释放资源  
        openssl_free_key($res);  
        //base64编码签名  
        $signBase64 = base64_encode($sign);  
        //url编码签名  
       // $signs = urlencode($signBase64);  
        return  $signBase64;  
    }  
	
     /** 
     * RSA验签 
     * @param $paramStr 
     * @param $sign 
     * @param $pubKey 
     * @return bool 
     */  
    public static function verify($paramStr, $sign, $pubKey)  {  
        //将字符串格式公私钥转为pem格式公私钥  
        $pubKeyPem = SignUtil::format_secret_key($pubKey, 'pub');  
        //转换为openssl密钥，必须是没有经过pkcs8转换的公钥  
        $res = openssl_get_publickey($pubKeyPem);
        //url解码签名
        //$signUrl = urldecode($sign);
        //base64解码签名  
        $signBase64 = base64_decode($sign);  
        //调用openssl内置方法验签，返回bool值  
        $result = (bool)openssl_verify($paramStr, $signBase64, $res);
        //释放资源  
        openssl_free_key($res);  
        //返回资源是否成功  
        return $result;  
    }  
    
	 /** 
     * 将字符串格式公私钥格式化为pem格式公私钥 
     * @param $secret_key 
     * @param $type 
     * @return string 
     */  
    public static function format_secret_key($secret_key, $type){  
        //64个英文字符后接换行符"\n",最后再接换行符"\n"  
        $key = (wordwrap($secret_key, 64, "\n", true))."\n";  
        //添加pem格式头和尾  
        if ($type == 'pub') {  
            $pem_key = "-----BEGIN PUBLIC KEY-----\n" . $key . "-----END PUBLIC KEY-----\n";
        }else if ($type == 'pri') {  
            $pem_key = "-----BEGIN RSA PRIVATE KEY-----\n" . $key . "-----END RSA PRIVATE KEY-----\n";  
        }else{  
            echo('公私钥类型非法');  
            exit();  
        }  
        return $pem_key;  
    }  
}




?>