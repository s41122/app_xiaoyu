<?php
namespace app\home\logic;

use think\Model;

class Sms extends Model {
    public function sendsSms($data) {
        $prefix = config('database.prefix');
        //获取该标信息
        $borr_result = db('borrow')->field('user_id,borrow_period,borrow_apr,account,borrow_type,name')->where(['borrow_nid' => $data['borrow_nid']])->find();
        $period_name = "个月";
        if ($borr_result["borrow_type"] == "day") {
            $period_name = "天";
        }
        $borr_username = model('user')->getUser(['user_id' => $borr_result['user_id']]);
        $send_sms['type'] = $data['type'];
        //通过类型 获取短信模板
        $type = $data['type'];
        $sql = "select p1.status as smsstatus,p1.type_content,p1.bz,p2.*,p3.sex,p4.phone as app_phone from `{$prefix}sms_type` as p1 left join  `{$prefix}users_info` as p2 on p2.user_id='" . $data['user_id'] . "' left join `{$prefix}approve_realname` as p3 on p3.user_id ='" . $data['user_id'] . "' left join `{$prefix}approve_sms` as p4 on p4.user_id='" . $data['user_id'] . "' and p4.status=1 where p1.nid='" . $type . "'";
        $ex = db()->query($sql);
        if (!$ex) {
            return null;
        }
        $result = $ex[0];

        $result['type_content'] = str_replace('[newusername]', $borr_username['username'], $result['type_content']);
        $result['type_content'] = str_replace('[newaccount]', $borr_result['account'], $result['type_content']);
        $result['type_content'] = str_replace('[newmonth]', $borr_result['borrow_period'] . $period_name, $result['type_content']);
        $result['type_content'] = str_replace('[newapr]', $borr_result['borrow_apr'], $result['type_content']);
        $result['type_content'] = str_replace('[newtitle]', $borr_result['name'], $result['type_content']);
        if ($result['sex'] == 1) {
            $result['type_content'] = str_replace('先生/女士', '会员', $result['type_content']);   //先生
        } else if ($result['sex'] == 2) {
            $result['type_content'] = str_replace('先生/女士', '会员', $result['type_content']);
        }

        $send_sms['contents'] = $result['type_content'];
        $send_sms['phone'] = $result['app_phone'];

        if ($send_sms['phone'] != "") {
            $send_sms['user_id'] = $data['user_id'];
            $send_sms['send_code'] = $data['smstype'];
            if ($result['smsstatus'] == 1 && $data['user_id'] != $borr_result['user_id']) {
                $send_sms['status'] = $result['smsstatus'];
                model('approve', 'logic')->SendSMS($send_sms);
            } else {
                $_result['phone_result'] = '未开启短信提醒';
            }
        }
    }
}