<?php
namespace app\home\logic;

use think\Model;

class Message extends Model {
    function SendMessages($data = []){
        $data ['addtime'] = time();
        $data ['addip'] = get_client_ip();
        return $this->insert($data);
    }
}