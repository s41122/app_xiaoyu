<?php
namespace app\home\logic;

use think\Model;

class Remind extends Model {
    function sendRemind($data = []) {

        if (isset($data['receive_user']) && $data['receive_user'] != "") {
            $data['receive_userid'] = $data['receive_user'];
        }
        //判断是否存在
        $_log_result = model('remind_log')->getOne(['remind_nid' => $data['remind_nid']]);
        if ($_log_result == false) {
            $_log["user_id"] = $data["receive_userid"];
            $_log["nid"] = $data["nid"];
            $_log["remind_nid"] = $data["remind_nid"];
            $_log["code"] = $data["code"];
            $_log["article_id"] = $data["article_id"];
            $_log["title"] = $data["title"];
            $_log["content"] = $data["content"];
            $remind_log_id = model('remind_log')->add($_log);
        }

        //只发送站内信,不再判断用户是否愿意接受消息
        $message['send_userid'] = "0";
        $message['user_id'] = $data['receive_userid'];
        $message['name'] = $data['title'];
        $message['contents'] = $data['content'];
        $message['type'] = 'user';
        $message['status'] = isset($data['status']) ? $data['status'] : 1;
        $message_id = model('message')->add($message);
    }
}