<?php
namespace app\home\logic;

use think\Model;
use think\db;

class Auth extends Model {
    //送积分
    function auth($data = []) {
        if (!$data['user_id']) {
            return ['code' => 0, 'msg' => '用户ID不能为空'];
        }
        if (!$data['realname']) {
            return ['code' => 0, 'msg' => '请输入真实姓名'];
        }
        if (!$data['card_id']) {
            return ['code' => 0, 'msg' => '请输入身份证号'];
        }
        if (!$data['account']) {
            return ['code' => 0, 'msg' => '请输入银行卡号'];
        }
//        if (!$data['phone']) {
//            return ['code' => 0, 'msg' => '请输入银行预留手机号码'];
//        }
        if (!$data['code']) {
            return ['code' => 0, 'msg' => '请选择开户行地址'];
        }
        $user = model('users_info')->where(['user_id' => $data['user_id']])->find();

        if ($user['realname_status'] == 1) {
            return ['code' => 0, 'msg' => '您已完成实名认证'];
        }
        if (!IsChinese($data['realname'])) {
            return ['code' => 0, 'msg' => '真实姓名只能为中文'];
        }
        $u = model('user')->where(['user_id' => $data['user_id']])->find();

//        $check_phone = model('phone', 'logic')->checkCode([
//                'user_id' => $data['user_id'],
//                'type' => 'smscode',
//                'phone' => $data['phone'],
//                'code' => $data['code']
//        ]);
//        if (!$check_phone) {
//            return ['code' => 0, 'msg' => '手机验证码错误'];
//        }

        if (!isIdCard($data['card_id'])) {
            return ['code' => 0, 'msg' => '身份证号码格式错误！'];
        }
        if (model('approve_realname')->getOne([
                'user_id' => ['neq', $data['user_id']],
                'card_id' => $data['card_id'],
                'status' => 1
        ])
        ) {
            return ['code' => 0, 'msg' => '身份证号已经存在！'];
        }

        if (!$data['account'] || strlen($data['account']) < 10) {
            return ['code' => 0, 'msg' => '请输入正确的银行卡号！'];
        }

//        if (!isPhone($data['phone'])) {
//            return ['code' => 0, 'msg' => '请输入正确的手机号码！'];
//        }
        $bank_info = model('bank_card', 'logic')::info($data['account']);
        if (empty($bank_info['bank'])) {
            return ['code' => 0, 'msg' => '不支持该卡号所在银行！'];
        }

        //开启事务
        Db::startTrans();

        //对接第三方实名认证接口
        $i_data['cust_nm'] = $data['realname'];
        $i_data['capAcntNo'] = $data['account'];
        $i_data['certif_id'] = $data['card_id'];
        $i_data['mobile_no'] = $u['username'];
        $i_data['bank'] = $bank_info['bank'];
        $i_data['city_id'] = $data['code'];
        $i_data['mchnt_txn_ssn'] = 'reg' . date('YmdHis', time());
        $i_result = model('fuyh5', 'logic')->regAction($i_data);

        if ($i_result['plain']['resp_code'] != '0000' && $i_result['plain']['resp_code'] != '5343') {
            Db::rollback();
            return ['code' => 0, 'msg' => $i_result['plain']['resp_desc']];
        }

        //修改真实姓名
        $data['status'] = 1;
        $data["verify_userid"] = 1;
        $data["remark"] = '系统自动认证！';
        $data['sex'] = get_sex($data['card_id']);
        $data['status'] = 1;

        //添加实名认证
        $re = model('approve', 'logic')->AddRealname($data);
        if ($re['code'] != 1) {
            Db::rollback();
            return $re;
        }

        //添加银行卡记录
        $bank_data = [
                'account' => $data['account'],
                'user_id' => $data['user_id'],
                'phone' => $u['username'],
                'bank_code' => $bank_info['bank'],
                'name' => $data['realname'],
                'id_card' => $data['card_id'],
                'status' => 1
        ];

        $id = model('account_users_bank')->AddUsersBank($bank_data);
        if (!$id) {
            Db::rollback();
            return ['code' => 0, 'msg' => '添加银行卡失败!'];
        }

        //实名认证送15积分-------------------------
        $score_arr = ['nid' => 'realname', 'user_id' => $data['user_id'], 'beizhu' => '实名认证所得积分'];
        model('account', 'logic')->sendScore($score_arr);

        $credit_log['user_id'] = $data['user_id'];
        $credit_log['nid'] = "realname";
        $credit_log['code'] = "approve";
        $credit_log['type'] = "approve";
        $credit_log['addtime'] = time();
        $credit_log['article_id'] = $id;
        $credit_log['remark'] = "实名认证所得积分";
        model('credit', 'logic')->ActionCreditLog($credit_log);
        //实名认证送15积分结束---------------------

        //实名认证 邀请人获得100积分---------------
        $user_invite = model('spreads_users')->getSpread(['user_id' => $data['user_id']]);
        if (!$user_invite) {
            $score_arr = ['nid' => 'invitee', 'user_id' => $user_invite['spreads_userid'], 'beizhu' => '被邀请用户实名认证所得积分'];
            model('account', 'logic')->sendScore($score_arr);
        }
        //实名认证 邀请人获得100积分结束-----------

        //更新手机验证码
//        model('phone', 'logic')->setCodeStatus([
//                'id' => $check_phone['id'],
//                'code_status' => 0
//        ]);
        Db::commit();
        return ['code' => 1, 'msg' => '实名认证成功'];
    }
}