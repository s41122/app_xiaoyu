<?php
namespace app\home\logic;

use think\Model;
use think\db;

class Withdraw extends Model {
    public function addWithdraw($data = []) {
        if (!isset($data['user_id'])) {
            return ['code' => 0, 'msg' => '用户ID不能为空'];
        }
        if (!isset($data['money']) || !is_numeric($data['money']) || $data['money'] <= 0) {
            return ['code' => 0, 'msg' => '提现金额必须大于0'];
        }
        $user_bank = model('account_users_bank')->getOne(['user_id' => $data['user_id']]);
        if (!$user_bank) {
            return ['code' => 0, 'msg' => '请先绑定银行卡'];
        }
        $user = model('user')->getUser(['user_id' => $data['user_id']]);

        if (!$user) {
            return ['code' => 0, 'msg' => '用户不存在'];
        }
        $account = model('account')->getOne(['user_id' => $data['user_id']]);
        $money = round($data['money'], 2);
        if ($money > $account['balance']) {
            return ['code' => 0, 'msg' => '余额不足'];
        }

        if ($user['paypassword'] != md5($data['paypwd'])) {
            return ['code' => 0, 'msg' => '支付密码错误'];
        }

        //获取本月免费提现次数
        $free_num = $account['free_num'];

        if ($money > ($account['balance'] - $account['balance_frost_today'])) {
            return ['code' => 0, 'msg' => '余额不足'];
        }

        $fee_result = $this->getFee([
                'free_num' => $free_num,
                'total' => $money,
                'balance_cash' => $account['balance_cash'],
        ]);
        $balance_cash = $money > $account['balance_cash'] ? $account['balance_cash'] : $money;
        $balance_frost = $money > $account['balance_cash'] ? ($money - $account['balance_cash']) : 0;

        $cash_data['user_id'] = $data['user_id'];
        $cash_data['status'] = 0;
        $cash_data['total'] = $money;
        $cash_data['account'] = $user_bank['account'];
        $cash_data['bank'] = $user_bank['bank'];
        $cash_data['branch'] = $user_bank['branch'];
        $cash_data['province'] = $user_bank['province'];
        $cash_data['city'] = $user_bank['city'];
        $cash_data['nid'] = time() . rand(100, 999) . $data['user_id'];
        $cash_data['fee'] = $fee_result['fee'];
        $cash_data['free'] = $fee_result['free'];
        $cash_data['balance_cash'] = $balance_cash;
        $cash_data['balance_frost'] = $balance_frost;
        $cash_data['credited'] = $money - $fee_result['fee'];

        Db::startTrans();

        $id = model('account_cash')->add($cash_data);
        if (!$id) {
            Db::rollback();
            return ['code' => 0, 'msg' => '提现失败'];
        }
        $log_info["user_id"] = $data['user_id'];//操作用户id
        $log_info["nid"] = "cash_" . $cash_data['nid'];//订单号
        $log_info["borrow_nid"] = "cash_" . $cash_data['nid'];//订单号
        $log_info["account_web_status"] = 0;//
        $log_info["account_user_status"] = 0;//
        $log_info["money"] = $cash_data['total'];//操作金额
        $log_info["income"] = 0;//收入
        $log_info["expend"] = 0;//支出
        //$log_info["balance_cash"] = -$data['total'];//可提现金额
        //$log_info["balance_frost"] = 0;//不可提现金额

        $log_info["balance_cash"] = $cash_data['balance_cash'] ? -$cash_data['balance_cash'] : -$cash_data['total'];//可提现金额
        $log_info["balance_frost"] = $cash_data['balance_frost'] ? -$cash_data['balance_frost'] : 0;//不可提现金额
        $log_info['fee'] = $cash_data['fee'];//手续费
        $log_info['free'] = $cash_data['free'];//是否免费提现
        $log_info["frost"] = $cash_data['total'];//冻结金额

        $log_info["await"] = 0;//待收金额
        $log_info["type"] = "cash";//类型
        $log_info["to_userid"] = 0;//付给谁
        $log_info["remark"] = "申请提现{$cash_data['total']}元";//备注
        $log_info["code"] = 'account';
        $log_info['code_type'] = '';
        $log_info['code_nid'] = '';
        $log_info['repay'] = '';

        model('account', 'logic')->AddLog($log_info);

        $ex = model('account')->edit(['user_id' => $data['user_id']], ['frost_cash' => ['exp', 'frost_cash+' . $cash_data['total']]]);
        if (!$ex) {
            Db::rollback();
            return ['code' => 0, 'msg' => '更新账户失败'];
        }

        //加入用户操作记录
        $user_log["user_id"] = $data['user_id'];
        $user_log["code"] = "account";
        $user_log["type"] = "cash";
        $user_log["operating"] = "require";
        $user_log["article_id"] = $id;
        $user_log["result"] = 1;
        $user_log["content"] = $log_info["remark"];
        model('users_log')->AddUsersLog($user_log);

        //申请提现发送短信提醒
        $user_log["user_id"] = $data['user_id'];
        $user_log["type"] = "apply_success";
        $user_log["smstype"] = "sms";
        $user_log["user_id"] = $data['user_id'];
        $user_log['account'] = $cash_data['total'];
        //model('sms', 'logic')->sendsSms($user_log);

        Db::commit();
        //金账户提现
        $payment_class = model('fuyh5', 'logic');
        $res= $payment_class->cash([
            'amt' => $money - $fee_result['fee'],
            'login_id' => $user['username'],
            'mchnt_txn_ssn' => $cash_data['nid'],
        ]);
        return $res;
    }

    public function getFee($data = []) {
        if ($data['total'] > $data['balance_cash']) {
            $f = $data['total'] - $data['balance_cash'];
            $f_un = $f * 0.5;  //未投可提金额-手续费
            $f_un = ceil($f_un) / 100;
            if ($data['free_num'] > 0) {
                $fee = $f_un < 2 ? 2 : $f_un;
                $free = 1;
            } else {
                $fee = ($data['balance_cash'] > 0 ? 2 : 0) + ($f_un < 2 ? 2 : $f_un);
                $free = 0;
            }
        } else {
            if ($data['free_num'] > 0) {
                $fee = 0;
                $free = 1;
            } else {
                $fee = 2;
                $free = 0;
            }
        }
        return ['fee' => $fee, 'free' => $free];
    }
}