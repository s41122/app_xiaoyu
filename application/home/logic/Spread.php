<?php
namespace app\home\logic;

use think\Model;

class Spread extends Model {
    public function addSpread($data = []) {
        if (!$data["user_id"] || !$data["spreads_userid"]) {
            return null;
        }
        //bof两用户不能互相成为推广人
        $check = model('spreads_users')->getSpread([
                'spreads_userid' => $data['user_id'],
                'user_id' => $data["spreads_userid"]
        ]);
        if ($check) {
            return null;
        }
        //eof

        $re = model('spreads_users')->addSpread([
                'user_id' => $data['user_id'],
                'spreads_userid' => $data["spreads_userid"],
                'type' => $data['type'] ?: 'reg'
        ]);
        return $re;
    }
}