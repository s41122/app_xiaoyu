<?php
namespace app\home\logic;

use think\Model;

class Credit extends Model {
    /**
     * 积分操作
     *
     * @return Array $data = array("code"=>"模块","user_id"=>"用户","type"=>"类型","article_id"=>"文章id","code"=>"","code"=>"",);
     */
    public function ActionCreditLog($data = []) {
        $_nid = explode(",", $data['nid']);

        //第一步先删除没有的积分记录
        db('credit_log')->where([
                'code' => $data['code'],
                'type' => $data['type'],
                'article_id' => $data['article_id'],
                'nid' => ['not in', $data['nid']]
        ])->delete();

        //第二步加入资金记录
        if (count($_nid) > 0) {
            foreach ($_nid as $key => $nid) {
                if ($nid != "") {
                    $result = db('credit_type')->where(['nid' => $nid])->find();
                    if (!$result) continue;
                    if (isset($data['value']) && $data['value'] > 0) {
                        if ($result['credit_type'] == 1) {
                            $_value = $data['value'] * $result['value_scale'] * 0.01;
                        } else {
                            $_value = $data['value'];
                        }
                    } elseif ($nid == "present") {
                        $_value = $data['value'];
                    } else {
                        $_value = $result['value'];
                    }
                    $result = db('credit_log')->where([
                            'code' => $data['code'],
                            'type' => $data['type'],
                            'article_id' => $data['article_id'],
                            'nid' => $nid,
                            'user_id' => $data['user_id']
                    ])->find();
                    if (!$result) {
                        db('credit_log')->insert([
                                'code' => $data['code'],
                                'type' => $data['type'],
                                'article_id' => $data['article_id'],
                                'nid' => $nid,
                                'user_id' => $data['user_id'],
                                'value' => $_value,
                                'credit' => $_value,
                                'addtime' => time(),
                                'update_time' => time(),
                                'remark' => $data['remark']
                        ]);
                    } else {
                        db('credit_log')->where([
                                'code' => $data['code'],
                                'type' => $data['type'],
                                'article_id' => $data['article_id'],
                                'nid' => $nid,
                        ])->update([
                                'user_id' => $data['user_id'],
                                'value' => $_value,
                                'addtime' => time(),
                                'update_time' => time(),
                        ]);
                    }
                }
            }
            $result = $this->ActionCredit(array("user_id" => $data['user_id']));
        }
        return $result;
    }

    public function ActionCredit($data = []) {
        $prefix = config('database.prefix');
        $sql = "select sum(p1.credit) as num,p2.class_id from `{$prefix}credit_log` as p1 left join `{$prefix}credit_type` as p2 on p1.nid=p2.nid  where p1.user_id='{$data['user_id']}' group by p2.class_id order by p2.class_id desc";
        $result = db()->query($sql);
        $credits = serialize($result);
        $sql = "select 1 from `{$prefix}credit` where user_id='{$data['user_id']}'";
        $result = db()->query($sql);
        if (!$result) {
            $sql = "insert into `{$prefix}credit` set user_id='{$data['user_id']}',`credits`='{$credits}'";
        } else {
            $sql = "update `{$prefix}credit` set `credits`='{$credits}' where user_id='{$data['user_id']}'";
        }
        db()->query($sql);

        if (isset($data['type']) && $data['type'] == "dyp2p") {
            $result = model('borrow', 'logic')->GetBorrowCredit(array("user_id" => $data['user_id']));
            $sql = "update `{credit}` set credit='{$result['credit_total']}' where user_id='{$data['user_id']}'";
            db()->query($sql);
        }
    }

    function GetRankList($data = []) {
        $prefix = config('database.prefix');
        $result = db('credit_rank')->alias('p1')
                ->field('p1.*,p2.nid as class_nid')
                ->join($prefix . 'credit_class p2', 'p1.class_id = p2.id')
                ->order('order by p1.id desc')
                ->select();
        return $result;
    }

    function GetUserCreditRank($data) {
        $integral = $data['credit'];
        $class = $data['class'];
        if ($integral == "" && $integral != "0") return "";
        $credit_rank = model('credit', 'logic')->GetRankList();
        if (!$credit_rank) {
            return null;
        }
        $_result = [];
        foreach ($credit_rank as $key => $value) {
            $_result[$value['class_nid']][] = $value;
        }
        if ($class == "") {
            $result = $_result[0];
        } else {
            $result = $_result[$class];
        }
        if (count($result) > 0) {
            foreach ($result as $key => $value) {
                if ($value['point1'] <= $integral && $value['point2'] >= $integral) {
                    return $value;
                } elseif ($integral <= 0 && $value['point2'] == 0) {
                    return $value;
                }
            }
        }
    }
}