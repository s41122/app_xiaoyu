<?php
namespace app\home\logic;

use think\Model;

class Approve extends Model {
    public function AddRealname($data = []) {
        if (!$data['user_id']) {
            return ['code' => 0, 'msg' => '用户ID不能为空'];
        }
        $result = model('approve_realname')->add([
                'user_id' => $data['user_id'],
                'realname' => $data['realname'],
                'card_id' => $data['card_id'],
                'sex' => $data['sex'],
                'verify_userid' => $data['verify_userid'],
                'verify_remark' => $data['remark'],
                'verify_time' => time(),
                'status' => $data['status']
        ]);

        if (!$result) {
            return ['code' => 0, 'msg' => '实名认证记录已存在'];
        }
        $user_info['user_id'] = $data["user_id"];
        $user_info['realname'] = $data["realname"];
        $user_info['realname_status'] = 1;
        $user_info['sex'] = $data['sex'];
        $re = model('users_info')->edit($user_info);
        if (!$re) {
            return ['code' => 0, 'msg' => '修改用户信息失败'];
        }

        //站内信提醒
        $remind['nid'] = "approve_realname_success";
        $remind['receive_userid'] = $data["user_id"];
        $remind['remind_nid'] = "approve_realname_success_" . $data["user_id"] . "_" . time();
        $remind['code'] = "approve";
        $remind['article_id'] = $data["user_id"];
        $remind['title'] = "实名认证审核成功";
        $remind['content'] = "您在平台上的实名认证已经审核通过。";
        $remind['status'] = $data['status'];
        model('remind', 'logic')->sendRemind($remind);

        //加入审核记录
        $_data["user_id"] = $data["user_id"];
        $_data["result"] = $data["status"];
        $_data["code"] = "approve";
        $_data["type"] = "realname";
        $_data["article_id"] = $data["user_id"];
        $_data["verify_userid"] = $data["verify_userid"];
        $_data["remark"] = $data["remark"];
        model('users_examines')->add($_data);

        //添加积分记录
        $credit_log['user_id'] = $data['user_id'];
        $credit_log['nid'] = "realname";
        $credit_log['code'] = "approve";
        $credit_log['type'] = "realname";
        $credit_log['addtime'] = time();
        $credit_log['article_id'] = $data['user_id'];
        $credit_log['remark'] = "实名认证通过所得积分";
        model('credit', 'logic')->ActionCreditLog($credit_log);

        return ['code' => 1, 'msg' => 'success'];
    }

    /**
     * 发送短信
     *
     * @param Array $data = array("type"=>"类型","type"=>"类型","user_id"=>"用户","phone"=>"电话","content"=>"内容","time"=>"发送时间");
     * @return Array
     */
    public static function SendSMS($data) {
        if (config('con_sms_status') == 0) {
            return ['code' => 0, 'msg' => '手机短信功能已关闭'];
        }
        $_sms_url = explode("?", htmlentitydecode(config('con_sms_url')));
        $http = $_sms_url[0];

        if (config('con_sms_text')) {
            $data['contents'] = $data['contents'] . "【" . config('con_sms_text') . "】";
        }
        $contents = $data['contents'];
        $contents = URLEncode($contents);
        $_data = str_replace("#phone#", $data['phone'], $_sms_url[1]);
        $_data = str_replace("#content#", $contents, $_data);
        $__data = explode("&", $_data);
        foreach ($__data as $key => $value) {
            $_val = explode("=", $value);
            $_res[$_val[0]] = $_val[1];
        }
        $result = self::urlSMS($http . "?" . $_data); //url方式提交
        $data["send_return"] = $result;// $result;
        $data["send_url"] = $http . "?" . $_data;
        $result = model('approve_smslog')->add($data);
        if ($result) {
            return true;
        }
        return false;
    }
}