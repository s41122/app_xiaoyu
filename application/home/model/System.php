<?php
namespace app\common\model;

use think\Model;

class System extends Model
{
    public function getList($where = []) {
        $list = $this->where($where)->select();
        return $list;
    }
}
