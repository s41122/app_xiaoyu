<?php
namespace app\home\model;

use think\Model;

class CouponUsers extends Model {

    public function add($data = []) {
        $data['addtime'] = time();
        $data['modtime'] = time();
        $data['status'] = 1;
        $data['from'] = 2;
        $re = $this->data($data)->insert($data);
        return $re;
    }

    //获取用户红包信息
    public function getOne($data = []) {
        $where = [];
        if (isset($data['user_id'])) {
            $where ['p1.user_id'] = $data['user_id'];
        }
        if (isset($data['status'])) {
            $where ['p1.status'] = $data['status'];
        }
        if (isset($data['borrow_nid'])) {
            $where ['p1.borrow_nid'] = $data['borrow_nid'];
        }
        if (isset($data['borrow_tender_id'])) {
            $where ['p1.borrow_tender_id'] = $data['borrow_tender_id'];
        }
        $result = $this->alias('p1')->field('p2.deadline,p2.money,p2.min,p2.min_day,p2.interest,p2.style')
                ->join('__COUPON__ p2', 'p1.coupon_id=p2.id', 'LEFT')
                ->where($where)->find();
        return $result;
    }

    //修改
    public function edit($where = [], $data = []) {
        $data['modtime'] = time();
        $data['usetime'] = time();
        $re = $this->where($where)->update($data);
        return $re;
    }
}
