<?php
namespace app\home\model;

use think\Model;

class BaofuBind extends Model {
    function getOne($where, $order = 'id desc') {
        $result = $this->where($where)->order($order)->find();
        return $result;
    }

    function getList($where) {
        $result = $this->where($where)->select();
        return $result;
    }

    function add($data = []) {
        $data['from'] = 2;
        $data['create_time'] = time();
        $data['update_time'] = time();
        $data['status'] = 1;
        $this->insert($data);
        return $this->getLastInsID();
    }
}