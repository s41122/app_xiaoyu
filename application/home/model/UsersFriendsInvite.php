<?php
namespace app\home\model;

use think\Model;

class UsersFriendsInvite extends Model {

    public function addFriendsInvite($user_id, $user_id_friend) {
        if (!$user_id || !$user_id_friend) {
            return null;
        }
        $re = $this->insert([
                'user_id' => $user_id_friend,
                'friends_userid' => $user_id,
                'addtime' => time(),
                'addip' => get_client_ip(),
                'status' => 1,
                'type' => 1
        ]);
        return $re;
    }
}