<?php
namespace app\home\model;

use think\Model;

class SpreadsUsers extends Model {
    public function getSpread($where) {
        $re = $this->where($where)->find();
        return $re;
    }
    public function addSpread($data = []) {
        if (!$this->getSpread(['user_id' => $data['user_id']])) {
            $data['addtime'] = time();
            $data['addip'] = get_client_ip();
            $data['from'] = 2;
            $re = $this->insert($data);
            return $re;
        }
        return false;
    }
}