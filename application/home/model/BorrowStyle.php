<?php
namespace app\home\model;

use think\Model;

class BorrowStyle extends Model {
    public function getList($where, $order = ' id desc') {
        $list = $this->where($where)->order($order)->select();
        return $list;
    }
    public function getOne($where = []) {
        $re = $this->where($where)->find();
        return $re;
    }
}
