<?php
namespace app\home\model;

use think\Model;

class Coupon extends Model {
    public function getList($where, $order = ' id desc') {
        $list = $this->where($where)->order($order)->select();
        return $list;
    }
    public function getOne($coupon_id) {
        if (!$coupon_id) {
            return null;
        }
        $re = $this->where(['id' => $coupon_id])->find();
        return $re;
    }
    public function send($data = []) {
        if (!$data['coupon_id'] || !$data['user_id']) {
            return null;
        }
        $coupon = $this->getOne($data['coupon_id']);

        if ($coupon["status"] != 1){
            return ['code' => 0, 'msg' => '红包状态不正确'];
        }

        if ($coupon["type"] != 3 ){
            $data["exptime"] = $coupon["deadline"]>0?strtotime("+" . $coupon["deadline"] . " day"):0;
        }
        $this->where(['id' => $data['coupon_id']])->setInc('sendnum');
        model('coupon_users')->add([
            'mark' => $data['mark'],
            'exptime' => $data["exptime"],
            'coupon_id' => $data["coupon_id"],
            'user_id' => $data['user_id']
        ]);
        return true;
    }
}
