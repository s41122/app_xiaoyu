<?php
namespace app\home\model;

use think\Model;

class UsersFriends extends Model {

    public function addFriends($user_id, $user_id_friend) {
        if (!$user_id || !$user_id_friend) {
            return null;
        }
        $ip = get_client_ip();
        $re = $this->insertAll([
                ['user_id' => $user_id, 'friends_userid' => $user_id_friend, 'addtime' => time(), 'addip' => $ip, 'status' => 1],
                ['user_id' => $user_id_friend, 'friends_userid' => $user_id, 'addtime' => time(), 'addip' => $ip, 'status' => 1]
        ]);
        return $re;
    }

}
