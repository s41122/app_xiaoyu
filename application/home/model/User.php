<?php
namespace app\home\model;

use think\Model;

class User extends Model {
    protected $name = 'users';

    public function getUser($where) {
        $user = $this->where($where)->find();
        return $user;
    }

    public function updateUserLogin($user_id) {
        if (!$user_id) {
            return false;
        }
        $ex = $this->where(['user_id' => $user_id])->update([
                'logintime' => ['exp', 'logintime+1'],
                'up_time' => ['exp', 'last_time'],
                'last_time' => time(),
                'up_ip' => ['exp', 'last_ip'],
                'last_ip' => get_client_ip()
        ]);
        return $ex;
    }

    public function addUser($data = []) {
        if (!$data['phone']) {
            return null;
        }
        $ip = get_client_ip();
        $insert = ['reg_time' => time(),
                'reg_ip' => $ip,
                'up_time' => time(),
                'up_ip' => $ip,
                'last_time' => time(),
                'last_ip' => $ip,
                'username' => $data['phone'],
                'phone' => $data['phone'],
                'password' => '',
                'from' => 2
        ];
        if ($data['pwd'] != '') {
            $insert['password'] = md5($data['pwd']);
        }
        $this->insert($insert);
        $user_id = $this->getLastInsID();
        return $user_id;
    }

    public function editPwd($user_id, $pwd) {
        return $this->where(['user_id' => $user_id])->update([
                'password' => $pwd,
                'update_time' => time()
        ]);
    }
    public function editPaypwd($user_id, $pwd) {
        return $this->where(['user_id' => $user_id])->update([
                'paypassword' => $pwd,
                'update_time' => time()
        ]);
    }

    //根据手机号或邀请码获取邀请人
    public function getInviter($code) {
        if (!$code) {
            return null;
        }
        $re = $this->alias('a')->field('a.user_id')
                ->join('__USERS_INFO__ b', 'a.user_id = b.user_id', 'RIGHT')->where([
                'a.username|b.invite_code' => $code
        ])->find();
        return $re['user_id'];
    }
    public function updateUserInfo($user_id) {
        if (!$user_id) {
            return false;
        }
        $ex = $this->where(['user_id' => $user_id])->update([
                'logintime' => ['exp', 'logintime+1'],
                'up_time' => ['exp', 'last_time'],
                'last_time' => time(),
                'up_ip' => ['exp', 'last_ip'],
                'last_ip' => get_client_ip()
        ]);
        return $ex;
    }

    public function getUserInfo($user_id, $field = '*') {
        if (!$user_id) {
            return false;
        }
        $ex = $this->alias('a')->field($field)
                ->join('__USERS_INFO__ b', 'a.user_id = b.user_id', 'LEFT')
                ->join('__ACCOUNT_USERS_BANK__ c', 'a.user_id = c.user_id', 'LEFT')
                ->where(['a.user_id' => $user_id])->find();
        return $ex;
    }
}
