<?php
namespace app\home\model;

use think\Model;

class AccountUsersBank extends Model {
    function AddUsersBank($data = []) {
        $this->where(['user_id' => $data['user_id']])->delete();
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $this->insert($data);
        return $this->getLastInsID();
    }
    function getOne($where = []) {
        $re = $this->where($where)->find();

        $re['bank'] = model('bank_card', 'logic')::getBankName($re['bank_code']);

        return $re;
    }
}