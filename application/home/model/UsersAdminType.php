<?php
namespace app\home\model;

use think\Model;

class UsersAdminType extends Model
{
    public function getList($data = []) {
        $list = $this->order('`order` desc,id desc');
        if (isset($data['page'])) {
            $list = $list->page($data['page']);
        }
        if (isset($data['epage'])) {
            $list = $list->limit($data['epage']);
        }
        $list = $list->select();
        return $list;
    }
}
