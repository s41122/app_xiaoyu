<?php
namespace app\home\model;

use think\Model;

class AccountRecharge extends Model {
    function getOne($where) {
        $result = $this->where($where)->find();
        return $result;
    }

    function getList($where, $order = 'a.addtime desc') {
        $result = $this->field('a.*, b.name')->alias('a')
                ->join('__ACCOUNT_PAYMENT__ b', 'a.payment = b.id', 'LEFT')->where($where)->order($order)->select();
        return $result;
    }

    function add($data = []) {
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }

    function edit($where = [], $data = []) {
        $data['verify_time'] = time();
        $ex = $this->where($where)->update($data);
        return $ex;
    }
}