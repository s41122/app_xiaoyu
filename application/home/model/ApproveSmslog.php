<?php
namespace app\common\model;

use think\Model;

class ApproveSmslog extends Model
{
    public function add($data = [])
    {
        if (!$data) {
            return null;
        }
        $data['status'] = 0;
        $data['send_status'] = 0;
        $data['code_status'] = 0;
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }

    //获取最新一条记录
    public function getOne($data = [])
    {
        $where['phone'] = $data['phone'];
        $where['code'] = $data['code'];
        $where['code_status'] = 0;
        return $this->where($where)->order('id desc')->find();
    }
}