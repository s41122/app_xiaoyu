<?php
namespace app\home\model;

use think\Model;

class Articles extends Model {
    function getOne($where = []) {
        return $this->where($where)->find();
    }

    function getList($data = []) {
        $_sql = "where 1=1 ";

        //判断用户id
        if (isset($data['user_id'])) {
            $_sql .= " and p1.user_id = {$data['user_id']}";
        }

        //搜到用户名
        if (isset($data['username'])) {
            $_sql .= " and p2.username like '%{$data['username']}%'";
        }

        //搜到用户名
        if (isset($data['name'])) {
            $_sql .= " and p1.`name` like \"%{$data['name']}%\"";
        }
        //判断用户id
        if (isset($data['public'])) {
            $_sql .= " and p1.public = {$data['public']}";
        }
        if (isset($data['status'])) {
            $_sql .= " and p1.status = {$data['status']}";
        }

        if (isset($data['isfaq'])) {
            $_sql .= " and p1.isfaquestion = {$data['isfaq']}";
        }
        //搜到用户名
        if (isset($data['type_id'])) {
            $_sql .= " and FIND_IN_SET('{$data['type_id']}',p1.type_id)";
        }

        $_order = " order by p1.publish desc ";
        if (isset($data['order'])) {
            if ($data['order'] == "id_desc") {
                $_order = " order by p1.id desc ";
            } elseif ($data['order'] == "id_asc") {
                $_order = " order by p1.id asc ";
            } elseif ($data['order'] == "order_desc") {
                $_order = " order by p1.`order` desc ,p1.id desc";
            } elseif ($data['order'] == "order_asc") {
                $_order = " order by p1.`order` asc,p1.id desc";
            }
        }

        $prefix = config('database.prefix');
        $_select = " p1.*,p0.name as type_name,p0.nid as type_nid,p2.username,p3.fileurl";
        $sql = "select SELECT from `{$prefix}articles` as p1 
				  left join {$prefix}articles_type as p0 on p1.type_id=p0.id
				  left join {$prefix}users as p2 on p1.user_id=p2.user_id
				 left join {$prefix}users_upfiles as p3 on p1.litpic=p3.id
				 SQL ORDER LIMIT
				";

        //是否显示全部的信息
        if (isset($data['limit'])) {
            if ($data['limit'] != "all") {
                $_limit = "  limit " . $data['limit'];
            }
            $result = db()->query(str_replace(array('SELECT', 'SQL', 'ORDER', 'LIMIT'), array($_select, $_sql, $_order, $_limit), $sql));
            return $result;
        }

        //分页返回结果
        $data['page'] = isset($data['page']) && $data['page'] > 0 ? $data['page'] : 1;
        $data['epage'] = isset($data['epage']) && $data['epage'] > 0 ? $data['epage'] : 10;
        $_limit = " limit " . ($data['epage'] * ($data['page'] - 1)) . ", {$data['epage']}";
        $list = db()->query(str_replace(array('SELECT', 'SQL', 'ORDER', 'LIMIT'), array($_select, $_sql, $_order, $_limit), $sql));
        return $list;
    }

    function getDetail($data = []) {
        if (!isset($data['id']) || !is_numeric($data['id'])) {
            return false;
        }
        if (isset($data['hits_status'])) {
            $this->where(['id' => $data['id']])->setInc('hits');
        }
        $result = $this->where(['id' => $data['id']])->find();
        if (!$result) {
            return false;
        }
        //$result['contents'] = stripslashes($result['contents']);
        //$result['contents'] = html_entity_decode($result['contents']);
        return $result;
    }

    /**
     * 获得类型类别列表
     * $data = array("user_id"=>"用户id","username"=>"用户名");
     * @return Array
     */
    static function getTypeList($data = []) {

        $_sql = "where 1=1 ";
        //判断用户id
        if (isset($data['pid'])) {
            $_sql .= " and p1.pid = {$data['pid']}";
        }
        if (isset($data['type_id'])) {
            $_sql .= " and p1.id in({$data['type_id']})";
        }
        $_order = " order by p1.order desc ,p1.id asc ";

        $prefix = config('database.prefix');
        $_select = " p1.*";
        $sql = "select SELECT from `{$prefix}articles_type` as p1  SQL ORDER LIMIT ";

        //是否显示全部的信息
        if (isset($data['limit'])) {
            if ($data['limit'] != "all") {
                $_limit = "  limit " . $data['limit'];
            } else {
                $_limit = "";
            }
            return db()->query(str_replace(array('SELECT', 'SQL', 'ORDER', 'LIMIT'), array($_select, $_sql, $_order, $_limit), $sql));
        }

        //分页返回结果
        $data['page'] = !isset($data['page']) ? 1 : $data['page'];
        $data['epage'] = !isset($data['epage']) ? 10 : $data['epage'];

        $_limit = " limit " . ($data['epage'] * ($data['page'] - 1)) . ", {$data['epage']}";
        $list = db()->query(str_replace(array('SELECT', 'SQL', 'ORDER', 'LIMIT'), array($_select, $_sql, $_order, $_limit), $sql));

        return $list;
    }
}