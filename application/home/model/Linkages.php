<?php
namespace app\home\model;

use think\Model;

class Linkages extends Model {
    public function getName($data = []) {
        $result = $this->alias('a')->field('a.name')
                ->join('__LINKAGES_TYPE__ b', 'a.type_id = b.id')
                ->where(['a.value' => $data['type'], 'b.nid' => $data['nid']])->find();
        return $result['name'];
    }
    public function getNameList($data = []) {
        $result = $this->alias('a')->field('a.name,a.value')
                ->join('__LINKAGES_TYPE__ b', 'a.type_id = b.id')
                ->where(['b.nid' => $data['nid']])->select();
        return $result;
    }
}