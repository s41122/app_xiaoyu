<?php
namespace app\home\model;

use think\Model;

class AccountLog extends Model {
    function getOne($where) {
        $result = $this->where($where)->find();
        return $result;
    }

    function getList($where) {
        $result = $this->where($where)->select();
        return $result;
    }

    function add($data = []) {
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $this->insert($data);
        return $this->getLastInsID();
    }

    function edit($where = [], $data = []) {
        $ex = $this->where($where)->update($data);
        return $ex;
    }
}