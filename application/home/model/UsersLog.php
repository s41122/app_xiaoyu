<?php
namespace app\home\model;

use think\Model;

class UsersLog extends Model
{
    public function AddUsersLog($data = []) {
        if (!$data) {
            return null;
        }
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }
}
