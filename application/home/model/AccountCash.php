<?php
namespace app\home\model;

use think\Model;

class AccountCash extends Model {
    function getOne($where) {
        $result = $this->where($where)->find();
        return $result;
    }

    function getList($where, $order = 'addtime desc') {
        $result = $this->where($where)->order($order)->select();
        return $result;
    }

    function add($data = []) {
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }

    function edit($where = [], $data = []) {
        $ex = $this->where($where)->update($data);
        return $ex;
    }
}