<?php
namespace app\home\model;

use think\Model;

class Scrollpic extends Model {
    public function getList($where = [], $field = [], $order = 'a.order asc, a.id desc') {
        $list = $this->alias('a')->join('__SCROLLPIC_TYPE__ b', 'a.type_id = b.id')->field($field)->where($where)->order($order)->select();
        return $list ?: false;
    }
    public function getOne($where = [], $field = []) {
        $list = $this->field($field)->where($where)->find();
        return $list;
    }
}