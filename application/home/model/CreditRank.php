<?php
namespace app\common\model;

use think\Model;

class CreditRank extends Model
{
    public function getOne($where = []){
        return $this->where($where)->find();
    }
    public function getList($where = []){
        return $this->where($where)->select();
    }
}