<?php
namespace app\home\model;

use think\Model;

class AccountPayment extends Model {
    function getOne($where) {
        $result = $this->where($where)->find();
        return $result;
    }

    function getList($where) {
        $result = $this->where($where)->select();
        return $result;
    }
}