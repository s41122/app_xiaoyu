<?php
namespace app\home\model;

use think\Model;

class UserAddress extends Model {
    public function getOne($where) {
        $res = $this->where($where)->find();
        return $res;
    }
    public function getList($where, $order = 'id desc', $field = '*') {
        $res = $this->where($where)->order($order)->field($field)->select();
        return $res;
    }
    public function edit($data = []) {
        $data['update_time'] = time();
        if (!isset($data['id'])) {
            $data['create_time'] = time();
            $data['system'] = 2;
            $this->insert($data);
            $res = $this->getLastInsID();
        } else {
            $res = $this->update($data);
        }
        return $res;
    }
}
