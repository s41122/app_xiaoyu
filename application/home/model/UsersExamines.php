<?php
namespace app\home\model;

use think\Model;

class UsersExamines extends Model {
    public function getOne($where = []) {
        return $this->where($where)->find();
    }
    public function add($data = []) {
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        return $this->insert($data);
    }
}