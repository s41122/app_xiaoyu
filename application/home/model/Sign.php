<?php
namespace app\home\model;

use think\Model;

class Sign extends Model {
    public function add($data = []) {
        $data['sign_year'] = date('Y');
        $data['sign_month'] = date('n');
        $data['sign_day'] = date('j');
        $data['createtime'] = time();
        $data['sign_date'] = date('Ymd');
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }
    //获取当日签到
    public function getSign($user_id) {
        $where = [
                'sign_year' => date('Y'),
                'sign_month' => date('m'),
                'sign_day' => date('d'),
                'user_id' => $user_id
        ];
        $result = $this->where($where)->count();
        return $result;
    }

    //获取当月累计签到次数
    public function getSignMonth($user_id) {
        $where = [
                'sign_year' => date('Y'),
                'sign_month' => date('m'),
                'user_id' => $user_id
        ];
        $result = $this->where($where)->count();
        return $result;
    }

    //获取累计签到次数
    public function getSignTotal($user_id) {
        $where = [
                'user_id' => $user_id
        ];
        $result = $this->where($where)->count();
        return $result;
    }
}