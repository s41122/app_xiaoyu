<?php
namespace app\common\model;

use think\Model;

class ApproveRealname extends Model {
    public function getOne($where = []) {
        $re = $this->where($where)->find();
        return $re;
    }

    public function add($data = []) {
        if (!$data['user_id']) {
            return null;
        }
        $result = $this->getOne(['user_id' => $data['user_id']]);
        if ($result['status'] == 1) {
            return null;
        }
        if ($result['status'] == 0) {
            $this->where(['user_id' => $data['user_id']])->delete();
        }
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $data['from'] = 2;
        $this->insert($data);
        return $this->getLastInsID();
    }
}