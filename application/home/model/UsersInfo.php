<?php
namespace app\home\model;

use think\Model;

class UsersInfo extends Model
{

    public function edit($data = []) {
        //判断是否存在
        $result = $this->where(['user_id' => $data['user_id']])->find();
        if (!$result) {
            $re = $this->insert($data);
        } else {
            $re = $this->where(['user_id' => $data['user_id']])->update($data);
        }
        return $re;
    }

    public function AddUsersLog($data = []) {
        $data ['addtime'] = time();
        $data ['addip'] = get_client_ip();
        $this->insert($data);
        return $this->getLastInsID();
    }

    public function getOne($where) {
        $result = $this->where($where)->find();
        return $result;
    }
}
