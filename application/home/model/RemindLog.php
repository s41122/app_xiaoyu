<?php
namespace app\common\model;

use think\Model;

class RemindLog extends Model
{
    public function getOne($where = [])
    {
        return $this->where($where)->find();
    }
    public function add($data = [])
    {
        $data['addtime'] = time();
        $data['addip'] = get_client_ip();
        $this->insert($data);
        return $this->getLastInsID();
    }
}