<?php
namespace app\home\model;

use think\Model;

class Version extends Model {
    protected $name = 'app_version';
    function getOne($where = [], $order = 'id desc') {
        $result = $this->where($where)->order($order)->find();
        return $result;
    }
    function getList($where, $order = 'id desc') {
        $result = $this->where($where)->order($order)->select();
        return $result;
    }
}