<?php
namespace app\home\model;

use think\Model;

class Borrow extends Model {
    public function getList($where = [], $field = []) {
        $field [] = 'borrow_end_time';
        $field [] = 'status';
        $field [] = 'borrow_type';
        $field [] = 'borrow_period';
        $field [] = 'borrow_apr';
        $field [] = 'borrow_apr_extra';
        $list = $this->field($field)->where($where)->order()->select();
        if (!$list) {
            return false;
        }

        foreach ($list as $key => $value) {
            //借款是否到期
            $borrow_end_status = 0;
            if ($value['status'] == 1 && $value['borrow_end_time'] < time()) {
                $borrow_end_status = 1;
            }
            $list[$key]["borrow_end_status"] = $borrow_end_status;
            $period_name = "个月";
            if ($value["borrow_type"] == "day") {
                $period_name = "天";

                $i = $value['borrow_period'] % 30;
                $day = ($value['borrow_period'] - $i) / 30;
                $list[$key]['day'] = $day;
            }

            $list[$key]["borrow_apr_1"] = $value["borrow_apr"];

            $list[$key]["borrow_period_name"] = $value["borrow_period"] . $period_name;

            $list[$key]['borrow_apr1'] = number_format($value['borrow_apr']-$value['borrow_apr_extra'],1);
        }
        return $list;
    }

    public function getOne($where = [], $field = []) {
        $field [] = 'borrow_end_time';
        $field [] = 'status';
        $field [] = 'borrow_type';
        $field [] = 'borrow_period';
        $field [] = 'borrow_apr';
        $field [] = 'borrow_apr_extra';
        $list = $this->field($field)->where($where)->find();
        foreach ($list as $key => $value) {
            //借款是否到期
            $borrow_end_status = 0;
            if ($value['status'] == 1 && $value['borrow_end_time'] < time()) {
                $borrow_end_status = 1;
            }
            $list[$key]["borrow_end_status"] = $borrow_end_status;
            $period_name = "个月";
            if ($value["borrow_type"] == "day") {
                $period_name = "天";

                $i = $value['borrow_period'] % 30;
                $day = ($value['borrow_period'] - $i) / 30;
                $list[$key]['day'] = $day;
            }

            $list[$key]["borrow_apr_1"] = $value["borrow_apr"];

            $list[$key]["borrow_period_name"] = $value["borrow_period"] . $period_name;

            $list[$key]['borrow_apr1'] = number_format($value['borrow_apr']-$value['borrow_apr_extra'],1);
        }
        return $list;
    }
}