<?php
namespace app\home\model;

use think\Model;

class UsersType extends Model
{
    public function getList($data = []) {
        $list = $this->order('checked desc,`order` desc,id desc');
        if (isset($data['page'])) {
            $list = $list->page($data['page']);
        }
        if (isset($data['epage'])) {
            $list = $list->limit($data['epage']);
        }
        $list = $list->select();
        return $list;
    }
}
