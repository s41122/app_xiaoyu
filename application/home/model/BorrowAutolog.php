<?php
namespace app\home\model;

use think\Model;

class BorrowAutolog extends Model {
    public function add($data = []) {
        $data ['addtime'] = time();
        $data ['addip'] = get_client_ip();
        $this->insert($data);
        return $this->getLastInsID();
    }
}